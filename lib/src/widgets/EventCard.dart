// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:eva_icons_flutter/eva_icons_flutter.dart';
// import 'package:atta_mobile_app/src/widgets/Animation.dart';
// import 'package:atta_mobile_app/Utils/FixedAssets.dart';

// import 'package:atta_mobile_app/src/data/models/EventModel.dart';
// // import 'package:atta_mobile_app/src/controllers/EventController.dart';
// import 'package:atta_mobile_app/src/data/models/EventsAttendes.dart';

// class EventCard extends StatefulWidget {
//   // EventModel eventModel;
//   bool favScreen = false;

//   EventCard({ this.favScreen});

//   @override
//   _EventCardState createState() => _EventCardState();
// }

// class _EventCardState extends State<EventCard>
//     with AutomaticKeepAliveClientMixin {
//   @override
//   bool get wantKeepAlive => true;
//   bool isLoading = false;
//   EventsAttendes eventsAttendes = EventsAttendes();
//   // EventModel evenfavourite = EventModel();
//   // EventController _eventController = EventController();
//   bool isDeleted = false;
//   bool isDeleting = false;

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);

//     return isDeleted && widget.favScreen
//         ? SizedBox()
//         : isDeleting && widget.favScreen
//             ? FadeIn(
//                 0,
//                 Stack(
//                   children: <Widget>[
//                     card(context),
//                     detailsBottom(
//                       context,
//                     )
//                   ],
//                 ),
//                 begin: 0.0,
//                 end: 300.0,
//                 begino: 1.0,
//                 endo: 0.0,
//               )
//             : Stack(
//                 children: <Widget>[
//                   card(context),
//                   detailsBottom(
//                     context,
//                   )
//                 ],
//               );
//   }

//   Widget detailsBottom(BuildContext context) => Container(
//         padding: EdgeInsets.only(top: 135),
//         margin: EdgeInsets.only(right: MediaQuery.of(context).size.width - 130),
//         child: RaisedButton(
//           shape: RoundedRectangleBorder(
//               side: BorderSide(color: Color(0xff793B8E)),
//               borderRadius: BorderRadius.all(Radius.circular(10.0))),
//           color: Colors.white,
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Text(
//               "المزيد",
//               // "احجز",
//               style: TextStyle(
//                   color: Color(0xff793B8E), fontWeight: FontWeight.bold),
//             ),
//           ),
//           onPressed: () {
//             // 
//           },
//         ),
//       );

//   Widget card(BuildContext context) => Container(
//         padding: EdgeInsets.all(8),
//         //height: 250,
//         child: Card(
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(8.0))),
//           child: Column(
//             children: <Widget>[
//               Container(
//                   height: 150,
//                   child: ClipRRect(
//                       borderRadius: BorderRadius.only(
//                         topLeft: Radius.circular(8),
//                         topRight: Radius.circular(8),
//                       ),
//                       child: widget.eventModel.imagePath != null
//                           ? Image.network(widget.eventModel.imagePath,
//                               fit: BoxFit.fitWidth,
//                               width: MediaQuery.of(context).size.width)
//                           : Image.asset(
//                               FixedAssets.logo,
//                               fit: BoxFit.fitWidth,
//                               width: MediaQuery.of(context).size.width,
//                             ))),
//               Row(
//                 children: <Widget>[
//                   Expanded(
//                     child: Padding(
//                       padding: const EdgeInsets.only(
//                           top: 25.0, left: 8, right: 8, bottom: 8),
//                       child: Column(
//                         children: <Widget>[
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               Expanded(
//                                   child: Text(
//                                       widget.eventModel.titleAr != null
//                                           ? widget.eventModel.titleAr
//                                           : "",
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.bold,
//                                           color: Color(0xff793B8E)))),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               // Icon(Icons.flag,size: 20,color: Colors.green,),
//                               // Text("المكان:",style: TextStyle(color:Color(0xff793B8E),fontSize: 10 ),),
//                               // Text(eventModel.eventAddressAr!=null? eventModel.eventAddressAr:"",style: TextStyle(color:Color(0xff793B8E),fontSize: 10 ),)
//                             ],
//                           ),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               // Expanded(
//                               //     child: Text(eventModel.titleAr!=null?eventModel.titleAr:"",
//                               //         style: TextStyle(
//                               //             fontWeight: FontWeight.bold,
//                               //             color: Color(0xff793B8E)))),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               Icon(
//                                 Icons.flag,
//                                 size: 20,
//                                 color: Colors.green,
//                               ),
//                               Text(
//                                 "المكان:",
//                                 style: TextStyle(
//                                     color: Color(0xff793B8E), fontSize: 10),
//                               ),
//                               Text(
//                                 widget.eventModel.eventAddressAr != null
//                                     ? widget.eventModel.eventAddressAr
//                                     : "",
//                                 style: TextStyle(
//                                     color: Color(0xff793B8E), fontSize: 10),
//                               )
//                             ],
//                           ),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               Expanded(
//                                   child: Text(
//                                       widget.eventModel.briefAr != null
//                                           ? widget.eventModel.briefAr
//                                           : "",
//                                       overflow: TextOverflow.ellipsis,
//                                       maxLines: 2,
//                                       style: TextStyle(color: Colors.grey)))
//                             ],
//                           ),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: <Widget>[
//                               Container(
//                                 child: Row(
//                                   children: <Widget>[
//                                     Icon(
//                                       Icons.date_range,
//                                       color: Colors.grey,
//                                       size: 20,
//                                     ),
//                                     Text(
//                                         widget.eventModel.formatedEventDate !=
//                                                 null
//                                             ? widget
//                                                 .eventModel.formatedEventDate
//                                             : "",
//                                         style: TextStyle(
//                                             color: Color(0xff793B8E),
//                                             fontSize: 13))
//                                   ],
//                                 ),
//                               ),
//                               //  SizedBox(
//                               //    width: MediaQuery.of(context).size.width-270,
//                               //  ),
//                               Container(
//                                 child: Row(
//                                   children: <Widget>[
//                                     widget.eventModel.isFavoriteLoading
//                                         ? IconButton(
//                                             icon: Icon(EvaIcons.loaderOutline,
//                                                 size: 20,
//                                                 color: widget.eventModel
//                                                             .isFavorite ==
//                                                         false
//                                                     ? Colors.red
//                                                     : Colors.grey),
//                                             onPressed: null)
//                                         : IconButton(
//                                             icon: Icon(Icons.favorite,
//                                                 size: 20,
//                                                 color: widget.eventModel
//                                                             .isFavorite ==
//                                                         true
//                                                     ? Colors.red
//                                                     : Colors.grey),
//                                             onPressed: () {
//                                               // _eventController
//                                               //     .toggleEventFavourite(
//                                               //         widget.eventModel.eventId,
//                                               //         isDetails: false);
//                                               if (widget.favScreen) {
//                                                 setState(() {
//                                                   isDeleting = true;
//                                                 });
//                                                 Timer(
//                                                     Duration(milliseconds: 500),
//                                                     () {
//                                                   setState(() {
//                                                     isDeleted = true;
//                                                   });
//                                                 });
//                                               }
//                                             },
//                                           ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ],
//                       ),
//                     ),
//                   )
//                 ],
//               )
//             ],
//           ),
//         ),
//       );
// }
