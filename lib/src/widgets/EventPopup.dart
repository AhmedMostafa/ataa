// import 'package:atta_mobile_app/src/data/models/EventModel.dart';
// import 'package:atta_mobile_app/src/data/models/EventsAttendes.dart';
// import 'package:atta_mobile_app/src/data/services/EventService.dart';
// import 'package:flutter/material.dart';

// import '../TextFiledController.dart';

// class EventPopup extends StatefulWidget {
//   EventModel eventModel;

//   EventPopup(this.eventModel);

//   @override
//   EventPopupState createState() => EventPopupState();
// }

// class EventPopupState extends State<EventPopup> {
//   bool _isLoading = false;
//   String eventVerificationCode;
//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(10),
//       ),
//       child: Container(
//           height: MediaQuery.of(context).size.height * 0.4,
//           width: MediaQuery.of(context).size.width * 0.4,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(20.0),
//           ),
//           child: Column(
//             children: <Widget>[
//               Stack(
//                 children: <Widget>[
//                   Container(
//                     height: MediaQuery.of(context).size.height * 0.4,
//                   ),
//                   Container(
//                     height: MediaQuery.of(context).size.height * 0.13,
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.only(
//                             topLeft: Radius.circular(10.0),
//                             topRight: Radius.circular(10.0)),
//                         color: Color(0xffFF4E00)),
//                   ),
//                   Center(
//                     child: Padding(
//                       padding: const EdgeInsets.symmetric(
//                           horizontal: 90.0, vertical: 90.0),
//                       child: TextField(
//                         controller: TextFieldController.noOfAttendes,
//                         keyboardType: TextInputType.number,
//                         textInputAction: TextInputAction.done,
//                         // controller: _textFieldController,
//                         decoration: InputDecoration(labelText: "عدد الأشخاص"),
//                       ),
//                     ),
//                   ),
//                   Positioned(
//                     top: 180.0,
//                     right: 95.0,
//                     child: _isLoading
//                         ? CircularProgressIndicator()
//                         : FlatButton(
//                             color: Color(0xffFF4E00),
//                             child: Text(
//                               "تأكيد",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.w500),
//                             ),
//                             onPressed: () async {
//                               setState(() {
//                                 _isLoading = true;
//                               });
//                               EventService _eventService = new EventService();
//                               EventsAttendes eventAttendes =
//                                   new EventsAttendes();
//                               eventAttendes.noOfAttendees = int.parse(
//                                   TextFieldController.noOfAttendes.text);
//                               print(widget.eventModel.eventId.toString() +
//                                   " 1111");
//                               eventAttendes.eventId = widget.eventModel.eventId;
// //                                            EventsAttendes savedModel =
//                               Map data = await _eventService
//                                   .reserveEvent(eventAttendes)
//                                   .then((value) {
//                                 this.eventVerificationCode = value;
//                                 print(value.toString() + " value of event");
//                               });
//                               print(data.toString() + " aa");
//                               setState(() {
//                                 _isLoading = false;
//                               });

//                               //Navigator.pop(context);
//                             },
//                           ),
//                   ),
//                   Positioned(
//                     top: 220,
//                     right: 95,
//                     child: Text(
//                       this.eventVerificationCode == null
//                           ? "لا يوجد"
//                           : this.eventVerificationCode,
//                       style: TextStyle(
//                           fontSize: 18.0,
//                           color: Colors.black,
//                           fontWeight: FontWeight.w600),
//                     ),
//                   ),
//                   Column(
//                     children: <Widget>[
//                       Padding(
//                         padding: const EdgeInsets.all(25.0),
//                         child: Text(
//                           "عدد الأشخاص الراغبين في الحضور",
//                           style: TextStyle(
//                               fontSize: 18.0,
//                               color: Colors.white,
//                               fontWeight: FontWeight.w600),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ],
//               )
//             ],
//           )),
//     );
//   }
// }
