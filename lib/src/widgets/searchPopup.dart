
// import 'package:atta_mobile_app/src/data/models/CityModel.dart';
// import 'package:flutter/material.dart';

// class SearchPopup extends StatefulWidget {
//   //List<CityModel> cities = new List<CityModel>();
//   // SearchController _searchController;
//   List<DropdownMenuItem<String>> _dropDownMenuItems = new List();
//   VoidCallback refreshView;

//   // SearchPopup(this._searchController, this.refreshView) {
//   //   for (int i = 0; i < _searchController.cities.length; i++) {
//   //     _dropDownMenuItems.add(new DropdownMenuItem(
//   //         value: _searchController.cities[i].nameAr,
//   //         child: new Text(_searchController.cities[i].nameAr)));
//   //   }
//     // cities = _searchController.cities;
//   }

//   SearchPopupState createState() => new SearchPopupState();
// }

// class SearchPopupState extends State<SearchPopup> {
//   List<DropdownMenuItem<String>> _dropDownMenuItems;

//   TextEditingController searchControlPopup = TextEditingController();
//   CityModel _currentCity;
//   List<CityModel> cities = new List<CityModel>();
//   @override
//   void initState() {
//     cities = widget._searchController.cities;
//     searchControlPopup.text = widget._searchController.searchWord;
//     _dropDownMenuItems = widget._dropDownMenuItems;
//     _currentCity = widget._searchController.selectedCity;
// //    for (int i = 0; i < cities.length; i++) {
// //      if (cities[i].isSelected == true) {
// //        _currentCity = cities[i];
// //      }
// //    }
// //    print(_currentCity.nameAr);
// //    _currentCity =
// //        _dropDownMenuItems[0].value;

//     //listType.add();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     var width = MediaQuery.of(context).size.width;
//     var height = MediaQuery.of(context).size.height;
//     return Container(
//       padding: EdgeInsets.only(
//           left: 10, right: 10, top: height / 5, bottom: width / 2),
//       child: Card(
//         shape: OutlineInputBorder(
//             borderRadius:
//                 new BorderRadius.all(new Radius.elliptical(10.0, 10.0))),
//         child: Container(
//           //height: (height / 4),
//           child: Padding(
//             padding: const EdgeInsets.all(10),
//             child: Column(
//               children: <Widget>[
//                 TextField(
//                   textInputAction: TextInputAction.search,
//                   controller: searchControlPopup,
//                   decoration: InputDecoration(
//                     border: OutlineInputBorder(
//                       borderRadius: BorderRadius.circular(10),
//                     ),
//                     labelText: "كلمه البحث",
//                     //    hintText: "بحث"
//                   ),
//                   //enabled: false,

//                   // onChanged: (value) => _coponController.searchCopons(value),
//                 ),
//                 SizedBox(
//                   height: height / 15,
//                 ),
//                 Row(
//                   children: <Widget>[
//                     SizedBox(
//                       width: MediaQuery.of(context).size.width / 10,
//                     ),
//                     Text("اختار البلد:"),
//                     SizedBox(
//                       width: MediaQuery.of(context).size.width / 5,
//                     ),
//                     DropdownButton(
//                       value: _currentCity.nameAr,
//                       items: _dropDownMenuItems,
//                       onChanged: (c) {
//                         print(c + " name " + _currentCity.nameAr + " name");
//                         setState(() {
//                           for (int i = 0; i < cities.length; i++) {
//                             cities[i].isSelected = false;
//                           }
//                           for (int i = 0; i < cities.length; i++) {
//                             if (c == cities[i].nameAr) {
//                               _currentCity = cities[i];
//                               _currentCity.isSelected = true;
//                             }
//                           }
//                         });
//                       },
//                     ),
//                   ],
//                 ),
//                 Padding(
//                     padding: EdgeInsets.only(left: 10, right: 10, top: 30),
//                     child: RaisedButton(
//                         child: Padding(
//                           padding: EdgeInsets.only(left: 30, right: 30),
//                           child: Text("بحث",
//                               style: new TextStyle(
//                                   fontSize:
//                                       20.0 //, fontFamily: FixedAssets.mainFont
//                                   )),
//                         ),
//                         textColor: Colors.white,
//                         color: Colors.lightBlue,
//                         onPressed: () {
//                           //AddToSchedule(context);
//                           setState(() {
//                             widget._searchController
//                                 .getSearchWord(searchControlPopup.text);
//                             widget._searchController.selectedCity =
//                                 _currentCity;
//                             widget._searchController.cities = cities;
//                             widget.refreshView();
//                             widget._searchController.getSearchValue();
//                             Navigator.pop(context);
//                           });
//                         },
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(10.0))))
//               ],
//             ),
//           ),
//         ),
//         // ),
//       ),
//     );
//   }
// }
