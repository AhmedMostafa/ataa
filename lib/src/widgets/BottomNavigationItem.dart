import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomNavigationItem {
  BottomNavigationBarItem buildBottomNavigationBarItem(
      String imagePath, String title) { 
    return BottomNavigationBarItem(
        icon: Stack(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 8.0, right: 6.0),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      imagePath,
                      height: 26.0,
                    ),
                    Text(title,
                        style: TextStyle(
                          color: Colors.black,
                        ))
                  ],
                )),
          ],
        ),
        activeIcon: Column(
          children: <Widget>[
            Image.asset(
              imagePath,
              height: 26.0,
              color: Colors.deepPurpleAccent,
            ),
            Text(title,
                style: TextStyle(
                  color: Colors.deepPurpleAccent,
                ))
          ],
        ),
        title: Text(" ",
            style: TextStyle(
              color: Colors.black,
            )));
  }
}
