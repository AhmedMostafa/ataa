import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SharedWidget {
  static onLoading(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () {
              return;
            },
            child: Center(child: SpinKitFadingCircle(
              itemBuilder: (_, int index) {
                return DecoratedBox(
                  decoration: BoxDecoration(
                    color: index.isEven ? Colors.red : Colors.green,
                  ),
                );
              },
            )),
          );
        });
  }

  static detailsBottom(BuildContext context, int casee //OfferModel offer
          ) =>
      Container(
        padding: EdgeInsets.only(top: 120),
        margin: EdgeInsets.only(right: MediaQuery.of(context).size.width - 200),
        width: 145,
        child: RaisedButton(
          color: Colors.white,
          shape: StadiumBorder(side: BorderSide(color: Colors.grey)),
          child: Row(
            children: <Widget>[
              Text("تفاصيل اكثر"),
              SizedBox(
                width: 11,
              ),
              Card(
                shape: CircleBorder(),
                color: Colors.green,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Icon(Icons.keyboard_arrow_left),
                ),
              ),
            ],
          ),
          onPressed: () {
            //   switch (casee) {
            //     case 1:
            //       {
            //         Navigator.push(
            //             context,
            //             MaterialPageRoute(
            //                 builder: (context) => ShopScreen(shopModel)));
            //       }
            //       break;
            //     case 2:
            //       {
            //        if (TextFieldController.isGust == false) {
            //     Navigator.push(
            //         context,
            //         MaterialPageRoute(
            //             builder: (context) => TicketDetailScreen(
            //                   offer: offer,
            //                 )));
            //   } else {
            //     TextFieldController.mustLoginView(context);
            //   }
            //       }
            //       break;
            //     default:
            //       {
            //         //statements;
            //       }
            //       break;
            //   }
          },
        ),
      );
  static loading(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );

    //  WillPopScope(
    //   onWillPop: () {},
    //   child: Center(child: SpinKitFadingCircle(
    //     itemBuilder: (_, int index) {
    //       return DecoratedBox(
    //         decoration: BoxDecoration(
    //           color: index.isEven ? Colors.red : Colors.green,
    //         ),
    //       );
    //     },
    //   )),
    // );
  }

  static void showSnackBar(GlobalKey<ScaffoldState> key, message) {
    final snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 2),
    );
    key.currentState.showSnackBar(snackBar);
  }

  static void showToastMsg(message, isRed, {int time = 5}) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: time,
        backgroundColor: isRed == true ? Colors.red : Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
    // key.currentState.showSnackBar(snackBar);
  }

  static noResult({int noResult = 0}) {
    return Center(
      child: Text(
        noResult == 1
            ? "لا توجد نتائج..."
            : noResult == 2 ? "جاري التحميل..." : "",
        style: TextStyle(fontSize: 25, color: Colors.grey.shade500),
      ),
    );
  }
}
