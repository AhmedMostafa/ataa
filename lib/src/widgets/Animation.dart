import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';

class FadeIn extends StatelessWidget {
  final double delay;
  final Widget child;
  final double begin;
  final double end;
  final double begino;
  final double endo;
  FadeIn(this.delay, this.child,
      {this.begin = 130, this.end = 0, this.begino = 0.0, this.endo = 1.0});

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track("opacity")
          .add(Duration(milliseconds: 500), Tween(begin: begino, end: endo)),
      Track("translateX").add(
          Duration(milliseconds: 500), Tween(begin: begin, end: end),
          curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      delay: Duration(milliseconds: (100 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(
            offset: Offset(animation["translateX"], 0), child: child),
      ),
    );
  }
}
