// import 'dart:typed_data';

// import 'package:flutter/material.dart';
// import 'package:multi_image_picker/multi_image_picker.dart';

// class AssetView extends StatefulWidget {
//   final int _index;
//   final Asset _asset;

//   AssetView(this._index, this._asset);

//   @override
//   State<StatefulWidget> createState() => AssetState(this._index, this._asset);
// }

// class AssetState extends State<AssetView> {
//   int _index = 0;
//   Asset _asset;
//   ByteData byteData;
//   AssetState(this._index, this._asset);

//   @override
//   void initState() {
//     super.initState();
//     _loadImage();
//   }

//   void _loadImage() async {
//     await this._asset.requestThumbnail(300, 300, quality: 50);
//     byteData = await this._asset.requestThumbnail(100, 100);
//     if (this.mounted) {
//       setState(() {});
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (null != byteData) {
//       return Image.memory(
//         byteData.buffer.asUint8List(),
//         fit: BoxFit.cover,
//         gaplessPlayback: true,
//         height: 150.0,
//       );
//     }

//     return Text(
//       '${this._index}',
//       style: Theme.of(context).textTheme.headline,
//     );
//   }
// }
