import 'dart:async';

import 'package:atta_mobile_app/Utils/FixedAssets.dart';

import 'package:atta_mobile_app/src/ProviderModel/CategoryProviderModel.dart';

import 'package:atta_mobile_app/src/data/models/ShopModel.dart';
import 'package:atta_mobile_app/src/helper/HexColor.dart';
import 'package:atta_mobile_app/src/screens/ShopScreen.dart';
import 'package:atta_mobile_app/src/widgets/Animation.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:provider/provider.dart';

class CompanyCard extends StatefulWidget {
  ShopModel shopModel;
  bool favScreen = false;

  CompanyCard({this.shopModel, this.favScreen});
  @override
  _CompanyCardState createState() => _CompanyCardState( this.shopModel,);
}

class _CompanyCardState extends State<CompanyCard>
// with AutomaticKeepAliveClientMixin
{
  @override
  bool get wantKeepAlive => true;
  ShopModel shopModel;
  CategoryProviderModel _CategoryProviderModel = CategoryProviderModel();

  _CompanyCardState(
    this.shopModel,
  );

  bool isDeleting = false;
  bool isDeleted = false;

  @override
  Widget build(BuildContext context) {
    // super.build(context);
    return isDeleted && widget.favScreen
        ? SizedBox()
        : isDeleting && widget.favScreen
            ? FadeIn(
                0,
                ticketCard(context, shopModel),
                begin: 0.0,
                end: 300.0,
                begino: 1.0,
                endo: 0.0,
              )
            : ticketCard(context, shopModel);
  }

  Widget ticketCard(BuildContext context, ShopModel shopModel) {
    return  Consumer<CategoryProviderModel>(

      builder: (context, modelCategoryProviderModel, child) =>
       Container(
        padding: EdgeInsets.only(right: 5, left: 5),
        child: InkWell(
          // onTap: () {
          //   Navigator.push(context,
          //       MaterialPageRoute(builder: (context) => ShopScreen(shopModel)));
          // },
          child: Card(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => ShopScreen(shopModel)));
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.shade100)),
                    child: FadeInImage(
                      width: MediaQuery.of(context).size.width * 0.25,
                      height: MediaQuery.of(context).size.width * 0.25,
                      image: shopModel.logoPic != null
                          ? NetworkImage(shopModel.logoPic)
                          : AssetImage(
                              FixedAssets.logo,
                            ),
                      placeholder: AssetImage(FixedAssets.logo),
                    ),
                  ),
                ),

                Flexible(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.31,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(shopModel.nameAr == null ? "" : shopModel.nameAr,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 19)),
                        Text(
                          shopModel.addressAr == null ? "" : shopModel.addressAr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.grey, fontSize: 11),
                        ),
                        StarRating(
                          size: MediaQuery.of(context).size.width * 0.25 / 7,
                          rating: shopModel.shopRate == null
                              ? 3.5
                              : shopModel.shopRate,
                          color: Colors.orange,
                          borderColor: Colors.grey,
                          starCount: 5,
                        ),
                      ],
                    ),
                  ),
                ),

                Container(
                  height: 75.0,
                  width: 1.0,
                  color: Colors.grey.shade100,
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0),
                ),
                Flexible(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 5, top: 2),
                        height: 75,
                        width: MediaQuery.of(context).size.width / 1,
                        child: Text(
                          shopModel.discountAmount.toString().trim() == ""
                              ? ""
                              : " خصم \n ${shopModel.discountAmount}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: HexColor("#006679"),
                              fontSize: 20.0,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      widget.shopModel.isFavoriteLoading
                          ? IconButton(
                              icon: Icon(EvaIcons.loaderOutline,
                                  size: 20,
                                  color: shopModel.isFavorite == false
                                      ? Colors.red
                                      : Colors.grey),
                              onPressed: null)
                          : IconButton(
                              icon: Icon(Icons.favorite,
                                  size: 20,
                                  color: shopModel.isFavorite == true
                                      ? Colors.red
                                      : Colors.grey),
                              onPressed: () {
                                setState(() {
                                  shopModel.isFavoriteLoading = true;
                                  if (shopModel.isFavorite == true) {
                                    shopModel.isFavorite = false;
                                    _CategoryProviderModel
                                        .deleteFavoritShop(shopModel.shopId);
                                  } else {
                                    shopModel.isFavorite = true;
                                    _CategoryProviderModel
                                        .saveFavoritShop(shopModel.shopId);
                                  }
                                  shopModel.isFavoriteLoading = false;
                                });
                                if (widget.favScreen) {
                                  setState(() {
                                    isDeleting = true;
                                  });
                                  Timer(Duration(milliseconds: 500), () {
                                    setState(() {
                                      isDeleted = true;
                                    });
                                  });
                                }
                              },
                            ),
                      // IconButton(
                      //   iconSize: 20,
                      //   icon: Icon(
                      //     Icons.favorite,
                      //     color: shopModel.isFavorite == true
                      //         ? Colors.red
                      //         : Colors.grey,
                      //   ),
                      //   onPressed: () {
                      //     setState(() {
                      //       if (shopModel.isFavorite == true) {
                      //         shopModel.isFavorite = false;
                      //         deleteFavoritShop(shopModel.shopId);
                      //       } else {
                      //         shopModel.isFavorite = true;
                      //         saveFavoritShop(shopModel.shopId);
                      //       }
                      //     });
                      //   },
                      // )
                    ],
                  ),
                ), // SizedBox(
                //   width: MediaQuery.of(context).size.width * 0.65,
                //   child: ListTile(
                //     isThreeLine: true,
                //     title: AutoSizeText(
                //         shopModel.nameAr == null ? "" : shopModel.nameAr,
                //         style:
                //             TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                //     subtitle: Column(
                //         crossAxisAlignment: CrossAxisAlignment.start,
                //         children: [
                //           Container(
                //             width: MediaQuery.of(context).size.width * 0.3,
                //             child: Text(
                //                 shopModel.addressAr == null
                //                     ? ""
                //                     : shopModel.addressAr,
                //                 maxLines: 2,
                //                 overflow: TextOverflow.ellipsis,
                //                 style: TextStyle(color: Colors.grey)),
                //           ),
                //           // AutoSizeText(
                //           //     shopModel.addressAr == null
                //           //         ? ""
                //           //         : shopModel.addressAr,
                //           //     maxLines: 1,
                //           //     overflow: TextOverflow.ellipsis,
                //           //     style: TextStyle(color: Colors.grey)),
                //           Row(
                //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //             children: <Widget>[
                //               StarRating(
                //                 size: MediaQuery.of(context).size.width * 0.25 / 6,
                //                 rating: shopModel.shopRate == null
                //                     ? 3.5
                //                     : shopModel.shopRate,
                //                 color: Colors.orange,
                //                 borderColor: Colors.grey,
                //                 starCount: 5,
                //               ),
                //               IconButton(
                //                 iconSize: 20,
                //                 icon: Icon(
                //                   Icons.favorite,
                //                   color: shopModel.isFavorite == true
                //                       ? Colors.red
                //                       : Colors.grey,
                //                 ),
                //                 onPressed: () {
                //                   setState(() {
                //                     if (shopModel.isFavorite == true) {
                //                       shopModel.isFavorite = false;
                //                       deleteFavoritShop(shopModel.shopId);
                //                     } else {
                //                       shopModel.isFavorite = true;
                //                       saveFavoritShop(shopModel.shopId);
                //                     }
                //                   });
                //                 },
                //               )
                //             ],
                //           ),
                //         ]),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
