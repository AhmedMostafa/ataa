import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:atta_mobile_app/src/data/models/OfferModel.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomeSlider extends StatelessWidget {
  String title;
  List<OfferModel> offerModels;
  bool isSlow;
  BuildContext homeContext;
  HomeSlider(this.title, this.offerModels, this.homeContext,
      {this.isSlow = false});

  @override
  Widget build(BuildContext context) {
    if (title == "أخر العروض") {
      HomeProviderModel.isOffer = true;
    } else {
      HomeProviderModel.isOffer = false;
    }

    return sliderContainer(context);
  }

  Widget sliderContainer(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Color(0xff793B8E)),
              ),
            )
          ],
        ),
        Center(
          child: Container(
            padding: EdgeInsets.all(8),
            height: 200,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: <Widget>[
                    carousel2(
                        context: context,
                        delayTime: HomeProviderModel.isOffer ? 500 : 900),
                  ],
                )),
          ),
        )
      ],
    );
  }

  Widget banner(BuildContext context, String name) => Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.black45,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            )),
        //width: MediaQuery.of(context).size.width / 2,
        child: Text(
          name != null ? name : "",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            shadows: <Shadow>[
              Shadow(
                offset: Offset(1.0, 1.0),
                blurRadius: 2.0,
                color: Colors.black.withOpacity(0.8),
              ),
            ],
          ),
        ),
      );

  Widget detailsBottom(BuildContext context, OfferModel offer) => Container(
        padding: EdgeInsets.only(top: 120),
        margin: EdgeInsets.only(right: MediaQuery.of(context).size.width - 200),
        width: 140,
        child: InkWell(
          child: Card(
            color: Colors.white,
            shape: StadiumBorder(side: BorderSide(color: Colors.grey)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: 10,
                ),
                Text(
                  "تفاصيل اكثر",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Card(
                  shape: CircleBorder(),
                  color: Colors.green,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Icon(
                      Icons.keyboard_arrow_left,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
          onTap: () {
            if (HomeProviderModel.isGust == false) {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => TicketDetailScreen(offer, false)));
            } else {
              HomeProviderModel.mustLoginView(homeContext);
            }
          },
        ),
      );

  Widget carousel() => new Carousel(
        boxFit: BoxFit.cover,
        showIndicator: false,
        images: [
          new AssetImage('assets/imgs/1.jpeg'),
          new AssetImage('assets/imgs/2.jpg'),
          new AssetImage('assets/imgs/3.jpg'),
        ],
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(seconds: 60),
      );

  static List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  Widget carousel2({BuildContext context, int delayTime}) => new CarouselSlider(
        items: map<Widget>(
          offerModels,
          (index, i) {
            return Container(
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(children: <Widget>[
                  FadeInImage(
                    image: i.offerPics != null && i.offerPics.length > 0
                        ? NetworkImage(i.offerPics[0].path)
                        : AssetImage(FixedAssets.logo),
                    placeholder: AssetImage(FixedAssets.logo),
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                  ),
                  banner(context, i.nameAr),
                  detailsBottom(context, i),
                  Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      // decoration: BoxDecoration(
                      //   gradient: LinearGradient(
                      //     colors: [
                      //       Color.fromARGB(200, 0, 0, 0),
                      //       Color.fromARGB(0, 0, 0, 0)
                      //     ],
                      //     begin: Alignment.bottomCenter,
                      //     end: Alignment.topCenter,
                      //   ),
                      // ),
                      padding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                      child: Text(
                        '',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 5.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
            );
          },
        ).toList(),
        autoPlayInterval: Duration(seconds: isSlow ? 4 : 3),
        autoPlay: offerModels.length != 1 ? true : false,
        autoPlayAnimationDuration: Duration(milliseconds: delayTime),
        viewportFraction: 1.0,
        aspectRatio: 40 / 20,
        enlargeCenterPage: true,
      );
}
