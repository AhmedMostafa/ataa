import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/LayoutProviderModel.dart';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/widgets/BottomNavigationItem.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:provider/provider.dart';
import 'SharedWidget.dart';

class LayoutScreen extends StatefulWidget {
  // LayoutScreen(Type userModel, UserModel model);

  @protected
  @override
  createState() => LayoutView();
}

class LayoutView extends State<LayoutScreen> {
  BottomNavigationItem bottomNavigationItem = BottomNavigationItem();

  // LayoutView() : super(LayoutProviderModel()) {
  //   layoutProviderModel = LayoutProviderModel.con;
  // }

  LayoutProviderModel layoutProviderModel;

//  @override
//  void deactivate() {}

  @override
  Widget build(BuildContext context) {
    return  Consumer<LayoutProviderModel>(

      builder: (context, layoutProviderModel, child) =>
     Scaffold(
        // key: TextFieldController.layoutScaffoldkey,
        endDrawer: MyDrawer(1, this.context),
        body: layoutProviderModel.screens[layoutProviderModel.currentTab],
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: layoutProviderModel.currentTab,
            onTap: (index) {
              setState(() {
                if (index != 2) {
                  // layoutProviderModel.setState(() {
                  //   layoutProviderModel.setCurrentTab(index);
                  // });

                  if (index == 1) {
                    HomeProviderModel.categoryID = 0;
                    HomeProviderModel.appBar = "كوبونات";
                  }
                  layoutProviderModel.setCurrentTab(index);

                  layoutProviderModel;
                  
                 
                  print(layoutProviderModel.currentTab.toString() + " tttt");
                } else {
                  print("asdasd");

//                TextFieldController.layoutScaffoldkey.currentState
//                    .openEndDrawer();

//                showDialog(
//                    context: context,
//                    builder: (context) {
//                      return Center(child: CircularProgressIndicator());
//                    });
                  if (HomeProviderModel.isGust == false) {
                    layoutProviderModel.setCurrentTab(index);

                    layoutProviderModel;
                    //Navigator.pushReplacementNamed(context, "/Search");
//                  Navigator.push(context,
//                      MaterialPageRoute(builder: (context) => SearchScreen()));
                  } else {
                    HomeProviderModel.mustLoginView(context);
                  }
//                Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SearchScreen()));
                }
              });
            },
            type: BottomNavigationBarType.fixed,
            items: <BottomNavigationBarItem>[
              bottomNavigationItem.buildBottomNavigationBarItem(
                  "assets/imgs/home.png", "الرئسيه"),
              bottomNavigationItem.buildBottomNavigationBarItem(
                  "assets/imgs/discount.png", "كوبونات"),
              bottomNavigationItem.buildBottomNavigationBarItem(
                  "assets/imgs/tag.png", "عروض"),
//            bottomNavigationItem.buildBottomNavigationBarItem(
//                "assets/imgs/search.png", "البحث"),
            ]),
      ),
    );
  }

  Widget appDrawer() => Drawer();
}

class MyDrawer extends StatelessWidget {
  // final GlobalKey<NavigatorState> navigator;
  final int pageNum;
  final contextPage;

  const MyDrawer(this.pageNum, this.contextPage); //: super(key: key);

  @override
  Widget build(BuildContext context) {
    print('[DEBUG] MyDrawer build');
    // print("Token is:" + TextFieldController.token);
    // print(
    // 'MemberInfoModel' + TextFieldController.userMemberModel.member.userId);
    var heightScreen = MediaQuery.of(context).size.height;
    mustLoginView() {
      showDialog(
          context: context,
          builder: (context) {
            return Center(child: CircularProgressIndicator());
          });
//      Navigator.pushReplacementNamed(context, "/Search");
      Navigator.pushReplacementNamed(context, "/MustLogin");
//      Navigator.pushReplacement(
//          context, MaterialPageRoute(builder: (context) => LoginScreen(true)));
    }

    return ClipRRect(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(50.0),
        bottomRight: Radius.circular(50.0),
      ),
      child: Container(
        height: heightScreen * 0.9,
        child: Drawer(
          elevation: 5.0,
          child: Directionality(
              textDirection: TextDirection.ltr,
              child: Container(
                decoration:
                    BoxDecoration(color: Color.fromRGBO(0, 104, 123, 1)),
                child: ListView(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: heightScreen / 100,
                        ),
                        HomeProviderModel.isGust == false
                            ? Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(0, 104, 123, 1)),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 25.0, vertical: 16),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.pushReplacementNamed(
                                          context, "/Profile");
                                    },
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 50.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ClipRRect(
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(15),
                                              topRight: Radius.circular(15),
                                            ),
                                            child: Container(
                                                width: 75.0,
                                                height: 75.0,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.rectangle,
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: HomeProviderModel
                                                                    .userPicture ==
                                                                ""
                                                            ? AssetImage(
                                                                HomeProviderModel
                                                                            .userPicture !=
                                                                        ""
                                                                    ? HomeProviderModel
                                                                        .userPicture
                                                                    : "assets/imgs/profile-placeholder.png",
                                                              )
                                                            : NetworkImage(
                                                                HomeProviderModel
                                                                    .userPicture)))),
                                            //Image.network('https://cdn.pixabay.com/photo/2018/02/09/21/46/rose-3142529__340.jpg',height: 80)
                                          ),
                                          Text(
                                            HomeProviderModel
                                                .memberInfoModel.nameEn,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0,
                                            ),
                                          ),
                                          Text(
                                            HomeProviderModel.userEmail,
                                            style: TextStyle(
                                                color: Colors.white
                                                    .withOpacity(0.5),
                                                fontSize: 15.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(0, 104, 123, 1)),
                                child: SizedBox(
                                  height: 80,
                                ),
                              ),
//                          GestureDetector(
//                            onTap: () {
//                              if (TextFieldController.isGust == true &&
//                                  pageNum != 1) {
//                                mustLoginView(context);
//                              } else {
//                                if (pageNum != 1) {
//                                  Navigator.pushReplacementNamed(
//                                      context, "/Home");
//                                }
//                              }
//                            },
//                            child:
                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(0, 104, 123, 1)),
                          height: MediaQuery.of(context).size.height * 0.66,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              ListTile(
                                  onTap: () {
                                    if (HomeProviderModel.isGust == true &&
                                        HomeProviderModel.notActivated &&
                                        pageNum != 1) {
                                      SharedWidget.showToastMsg(
                                          "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                          true);
                                      // mustLoginView();
                                    } else {
                                      if (pageNum != 1) {
                                        Navigator.pushReplacementNamed(
                                            context, "/Home");
                                      }
                                    }
                                  },
                                  title: null,
                                  leading: drawerItemLeading(
                                      context,
                                      pageNum == 1,
                                      "الرئيسيه",
                                      EvaIcons.homeOutline)

//                                  SizedBox(
//                                    width:
//                                        MediaQuery.of(context).size.width / 1.8,
//                                    child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.start,
//                                      crossAxisAlignment:
//                                          CrossAxisAlignment.start,
//                                      children: <Widget>[
//                                        ClipRRect(
//                                          borderRadius: BorderRadius.all(
//                                              Radius.circular(20)),
//                                          child: Container(
//                                              padding: EdgeInsets.all(5),
//                                              child: Padding(
//                                                padding: EdgeInsets.only(
//                                                    right:
//                                                        MediaQuery.of(context)
//                                                                .size
//                                                                .width /
//                                                            40,
//                                                    left: MediaQuery.of(context)
//                                                            .size
//                                                            .width /
//                                                        80),
//                                                child: Row(
//                                                  children: <Widget>[
//                                                    Icon(EvaIcons.homeOutline,
//                                                        color:
////                                                    pageNum != 1 ?
//                                                            Colors.white
////                                                        : Colors.black
//                                                        ),
//                                                    SizedBox(
//                                                        width: MediaQuery.of(
//                                                                    context)
//                                                                .size
//                                                                .width /
//                                                            14),
//                                                    Text("الرئيسيه",
//                                                        style: TextStyle(
//                                                            color:
//                                                                //pageNum != 1 ?
//                                                                Colors.white
////                                                            : Colors.black
//                                                            )),
//                                                  ],
//                                                ),
//                                              ),
//                                              decoration: BoxDecoration(
//                                                  color: pageNum != 1
//                                                      ? Color.fromRGBO(
//                                                          0, 104, 123, 1)
//                                                      : Color(0xff379aac))),
//                                        ),
//                                      ],
//                                    ),
//                                  ),
                                  ),
//                                ListTile(
//                                  title: Text("الرئيسيه",
//                                      style: TextStyle(
//                                          color: pageNum != 1
//                                              ? Colors.white
//                                              : Colors.black)),
//                                  onTap: () {
//                                    if (TextFieldController.isGust == true &&
//                                        pageNum != 1) {
//                                      mustLoginView(context);
//                                    } else {
//                                      if (pageNum != 1) {
//                                        Navigator.pushReplacementNamed(
//                                            context, "/Home");
//                                      }
//                                    }
//                                  },
//                                  leading: Icon(EvaIcons.homeOutline,
//                                      color: pageNum != 1
//                                          ? Colors.white
//                                          : Colors.black),
//                                ),
                              ListTile(
                                title: null,
//                                  Text("المفضل",
//                                      style: TextStyle(
//                                          color: pageNum != 2
//                                              ? Colors.white
//                                              : Colors.black)),
                                onTap: () {
                                  print(HomeProviderModel.isGust.toString() +
                                      " value");
                                  if (HomeProviderModel.isGust == true &&
                                      HomeProviderModel.notActivated &&
                                      pageNum != 2) {
                                    SharedWidget.showToastMsg(
                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                        true);
                                    // print(" issues test");
                                    // mustLoginView();
                                  } else {
                                    if (pageNum != 2) {
                                      print(" issues none");
                                      Navigator.pushReplacementNamed(
                                          context, "/Favorite");
                                    }
                                  }
                                },
                                leading: drawerItemLeading(
                                    context,
                                    pageNum == 2,
                                    "المفضل",
                                    EvaIcons.heartOutline),
//                                  Icon(EvaIcons.heartOutline,
//                                      color: pageNum != 2
//                                          ? Colors.white
//                                          : Colors.black),
                              ),
                              ListTile(
                                title: null,
//                                    Text("اخبار",
//                                        style: TextStyle(
//                                            color: pageNum != 3
//                                                ? Colors.white
//                                                : Colors.black)),
                                onTap: () {
                                  if (HomeProviderModel.isGust == true &&
                                      HomeProviderModel.notActivated &&
                                      pageNum != 3) {
                                    SharedWidget.showToastMsg(
                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                        true);
                                    // mustLoginView();
                                  } else {
                                    if (pageNum != 3) {
                                      Navigator.pushReplacementNamed(
                                          context, "/News");
                                    }
                                  }
                                },
                                leading: drawerItemLeading(
                                    context,
                                    pageNum == 3,
                                    "اخبار",
                                    Icons.chat_bubble_outline),
                              ),
//                                    Icon(Icons.chat_bubble_outline,
//                                        color: pageNum != 3
//                                            ? Colors.white
//                                            : Colors.black)),
                              ListTile(
                                title: null,
//                                  Text("فعاليات",
//                                      style: TextStyle(
//                                          color: pageNum != 4
//                                              ? Colors.white
//                                              : Colors.black)),
                                onTap: () {
                                  if (HomeProviderModel.isGust == true &&
                                      HomeProviderModel.notActivated &&
                                      pageNum != 4) {
                                    SharedWidget.showToastMsg(
                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                        true);
                                    // mustLoginView();
                                  } else {
                                    if (pageNum != 4) {
                                      Navigator.pushReplacementNamed(
                                          context, "/Event");
                                    }
                                  }
                                },
                                leading: drawerItemLeading(
                                    context,
                                    pageNum == 4,
                                    "فعاليات",
                                    EvaIcons.calendarOutline),
//                                  Icon(EvaIcons.calendarOutline,
//                                      color: pageNum != 4
//                                          ? Colors.white
//                                          : Colors.black),
                              ),
                              //trophy
//                              ListTile(
//                                title: null,
////                                  Text("فعاليات",
////                                      style: TextStyle(
////                                          color: pageNum != 4
////                                              ? Colors.white
////                                              : Colors.black)),
//                                onTap: () {
//                                  if (TextFieldController.isGust == true &&
//                                      TextFieldController.notActivated &&
//                                      pageNum != 5) {
//                                    SharedWidget.showToastMsg(
//                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
//                                        true);
//                                    // mustLoginView();
//                                  } else {
//                                    if (pageNum != 5) {
//                                      Navigator.pushReplacementNamed(
//                                          context, "/Trophy");
//                                    }
//                                  }
//                                },
//                                leading: drawerItemLeading(
//                                    context,
//                                    pageNum == 5,
//                                    "المسابقات",
//                                    EvaIcons.giftOutline),
////                                  Icon(EvaIcons.calendarOutline,
////                                      color: pageNum != 4
////                                          ? Colors.white
////                                          : Colors.black),
//                              ),
                              ListTile(
                                title: null,
//                                  Text("تواصل معنا",
//                                      style: TextStyle(
//                                          color: pageNum != 5
//                                              ? Colors.white
//                                              : Colors.black)),
                                onTap: () {
                                  if (HomeProviderModel.isGust == true &&
                                      HomeProviderModel.notActivated &&
                                      pageNum != 6) {
                                    SharedWidget.showToastMsg(
                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                        true);
                                    // mustLoginView();
                                  } else {
                                    if (pageNum != 6) {
//                                        Navigator.pop(context);
//                                        Navigator.pushNamed(context, "/About");
                                      Navigator.pushReplacementNamed(
                                          context, "/About");
                                    }
                                  }
                                },
                                leading: drawerItemLeading(
                                    context,
                                    pageNum == 6,
                                    "تواصل معنا",
                                    EvaIcons.personOutline),
//                                  Icon(EvaIcons.personOutline,
//                                      color: pageNum != 5
//                                          ? Colors.white
//                                          : Colors.black),
                              ),
                              ListTile(
                                title: null,
//                                  Text("تواصل معنا",
//                                      style: TextStyle(
//                                          color: pageNum != 5
//                                              ? Colors.white
//                                              : Colors.black)),
                                onTap: () {
                                  if (HomeProviderModel.isGust == true &&
                                      HomeProviderModel.notActivated &&
                                      pageNum != 7) {
                                    SharedWidget.showToastMsg(
                                        "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني.",
                                        true);
                                    // mustLoginView();
                                  } else {
                                    if (pageNum != 7) {
//                                        Navigator.pop(context);
//                                        Navigator.pushNamed(
//                                            context, "/Privacy");
                                      Navigator.pushReplacementNamed(
                                          context, "/Privacy");
                                    }
                                  }
                                },
                                leading: drawerItemLeading(
                                    context,
                                    pageNum == 7,
                                    "سياسة التطبيق",
                                    Icons.receipt),
//                                  Icon(EvaIcons.personOutline,
//                                      color: pageNum != 5
//                                          ? Colors.white
//                                          : Colors.black),
                              ),
                              HomeProviderModel.isGust == false
                                  ? ListTile(
                                      title: Text("تسجيل خروج",
                                          style: TextStyle(
                                              color: Colors.white
                                                  .withOpacity(0.6))),
                                      onTap: () async {
                                        String header = 'bearer ' +
                                            HomeProviderModel.token;
                                        // SharedWidget.loading(context);
                                        // print(header + ' header');

                                        // print(ApiRoutes.logout +
                                        //     TextFieldController.firebaseToken +
                                        //     ' firebase logout');
                                        SharedWidget.onLoading(context);
                                        print(ApiRoutes.logout +
                                            HomeProviderModel.firebaseToken);
                                        await http.get(
                                            ApiRoutes.logout +
                                                HomeProviderModel
                                                    .firebaseToken,
                                            headers: {
                                              "Authorization": header
                                            }).timeout(Duration(seconds: 4));
//                                        Navigator.pop(context);
                                        Navigator.pushReplacementNamed(
                                            contextPage, "/Splash");

                                        HomeProviderModel.isGust = true;
                                        HomeProviderModel.cleanUserInfo();
                                        SharedPreference.removeUserToken();
                                        CircularProgressIndicator();
                                      },
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        // ),
                      ],
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  Widget drawerItemLeading(context, isSelect, title, icon) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 1.8,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            child: Container(
                padding: EdgeInsets.all(5),
                child: Padding(
                  padding: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width / 40,
                      left: MediaQuery.of(context).size.width / 80),
                  child: Row(
                    children: <Widget>[
                      Icon(icon,
                          color:
//                                                    pageNum != 1 ?
                              Colors.white
//                                                        : Colors.black
                          ),
                      SizedBox(width: MediaQuery.of(context).size.width / 14),
                      Text(title,
                          style: TextStyle(
                              color:
                                  //pageNum != 1 ?
                                  Colors.white
//                                                            : Colors.black
                              )),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                    color: isSelect == false
                        ? Color.fromRGBO(0, 104, 123, 1)
                        : Color(0xff379aac))),
          ),
        ],
      ),
    );
  }
}
