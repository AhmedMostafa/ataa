import 'dart:convert';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'SharedWidget.dart';

class ChangePasswordPopup extends StatefulWidget {
  ChangePasswordPopupState createState() => ChangePasswordPopupState();
}

class ChangePasswordPopupState extends State<ChangePasswordPopup> {
  TextEditingController userPassword = TextEditingController();
  TextEditingController userConfirmPassword = TextEditingController();

  static final FocusNode focusPasswordField = new FocusNode();
  static final FocusNode focusRetypePasswordField = new FocusNode();

  String validateConfirmPassword(String val) {
    if (val.trim().isEmpty)
      return "من فضلك ادخل الرقم السرى";
    else if (val != userPassword.text) {
      return "الرقم السرى غير مطابق";
    } else
      return null;
  }

  String validatePassword(String val) {
    if (val.trim().isEmpty)
      return "من فضلك ادخل الرقم السرى";
    else if (val.length < 6) {
      return "الرقم السرى اقل من 6";
    } else
      return null;
  }

  static final GlobalKey<FormState> changePasswordKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(
          left: 10, right: 10, top: height / 5, bottom: height / 5),
      child: Card(
        shape: OutlineInputBorder(
            borderRadius:
                new BorderRadius.all(new Radius.elliptical(10.0, 10.0))),
        child: Container(
          //height: (height / 4),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Form(
              key: changePasswordKey,
              child: Column(
                children: <Widget>[
                  Text(
                    "تغير كلمه المرور",
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: height / 30,
                  ),
                  TextFormField(
                    controller: userPassword,
                    textInputAction: TextInputAction.next,
                    focusNode: focusPasswordField,
                    validator: (val) => validatePassword(val),
                    onFieldSubmitted: (value) {
                      FocusScope.of(context)
                          .requestFocus(focusRetypePasswordField);
                    },
                    obscureText: true,

                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: "كلمه المرور الجديده",
                    ),
                    // onChanged: (value) => _coponController.searchCopons(value),
                  ),
                  SizedBox(
                    height: height / 30,
                  ),
                  TextFormField(
                    controller: userConfirmPassword,
                    focusNode: focusRetypePasswordField,
                    onFieldSubmitted: (value) {
                      changePasswordApi(context);
                    },
                    obscureText: true,

                    validator: (val) => validateConfirmPassword(val),
                    textInputAction: TextInputAction.go,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: "تاكيد كلمه المرور الجديده",
                    ),

                    // onChanged: (value) => _coponController.searchCopons(value),
                  ),
                  SizedBox(
                    height: height / 15,
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 30),
                      child: RaisedButton(
                          child: Padding(
                            padding: EdgeInsets.only(left: 30, right: 30),
                            child: Text("حفظ",
                                style: new TextStyle(
                                    fontSize:
                                        20.0 //, fontFamily: FixedAssets.mainFont
                                    )),
                          ),
                          textColor: Colors.white,
                          color: Colors.lightBlue,
                          onPressed: () {
                            //AddToSchedule(context);

                            changePasswordApi(context);
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0))))
                ],
              ),
            ),
          ),
        ),
        // ),
      ),
    );
  }

  changePasswordApi(context) async {
    final form = changePasswordKey.currentState;
    if (form.validate()) {
      form.save();
      SharedWidget.onLoading(context);

      await http
          .post(
              //TextFieldController.apiUrl + "auth/register",
              ApiRoutes.changePassword,
              headers: {
                "Content-Type": "application/json",
                'Authorization': 'bearer ${HomeProviderModel.token}'
              },
              body: json.encode(userPassword.text.toString()))
          .then((response) {
        if (response.statusCode == 200) {
          Navigator.pop(context);

          Navigator.pop(context);
          Fluttertoast.showToast(
              msg: "تم تغير كلمه المرور بنجاح",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          Navigator.pop(context);
          Fluttertoast.showToast(
              msg: "خطأ في تغير كلمه المرور",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      });
    }
  }
}
