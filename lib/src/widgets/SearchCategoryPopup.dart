
import 'package:atta_mobile_app/src/ProviderModel/CategoryProviderModel.dart';

import 'package:atta_mobile_app/src/data/models/CityModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchCategoryPopup extends StatefulWidget {
  // List<CityModel> cities = new List<CityModel>();
  CategoryProviderModel _categoryController;
  BuildContext context;
//  final value = Provider.of<CategoryProviderModel>(context).cities;
  List<DropdownMenuItem<String>> _dropDownMenuItems = new List();
  VoidCallback refreshView;

  SearchCategoryPopup(this._categoryController, this.refreshView) {
    for (int i = 0; i < Provider.of<CategoryProviderModel>(context).cities.length; i++) {
      _dropDownMenuItems.add(new DropdownMenuItem(
          value: Provider.of<CategoryProviderModel>(context).cities[i].nameAr,
          child: new Text(Provider.of<CategoryProviderModel>(context).cities[i].nameAr)));
    }
    // cities =Provider.of<CategoryProviderModel>(context).cities;
  }

  SearchCategoryPopupState createState() => new SearchCategoryPopupState();
}

class SearchCategoryPopupState extends State<SearchCategoryPopup> {
  List<DropdownMenuItem<String>> _dropDownMenuItems;

  TextEditingController searchControlPopup = TextEditingController();
  CityModel _currentCity;
  List<CityModel> cities = new List<CityModel>();

//   @override
//   void initState() {
//     cities = widget._categoryController.cities;
//     searchControlPopup.text = widget._categoryController.searchWord;
//     _dropDownMenuItems = widget._dropDownMenuItems;
//     _currentCity = widget._categoryController.selectedCity;
// //    for (int i = 0; i < cities.length; i++) {
// //      if (cities[i].isSelected == true) {
// //        _currentCity = cities[i];
// //      }
// //    }
// //    print(_currentCity.nameAr);
// //    _currentCity =
// //        _dropDownMenuItems[0].value;

//     //listType.add();
//     super.initState();
//   }
  
bool isLoaded = false;
didChangeDependencies() {
  super.didChangeDependencies();
  if(isLoaded == false){
   
cities = widget._categoryController.cities;
    searchControlPopup.text = widget._categoryController.searchWord;
    _dropDownMenuItems = widget._dropDownMenuItems;
    _currentCity = widget._categoryController.selectedCity;
   isLoaded=true;
  }
 
}

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return  Consumer<CategoryProviderModel>(

      builder: (context, categoryProviderModel, child) =>
     Container(
        padding: EdgeInsets.only(
            left: 10, right: 10, top: height / 5, bottom: width / 2),
        child: Card(
          shape: OutlineInputBorder(
              borderRadius:
                  new BorderRadius.all(new Radius.elliptical(10.0, 10.0))),
          child: Container(
            //height: (height / 4),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  TextField(
                    textInputAction: TextInputAction.search,
                    controller: searchControlPopup,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: "كلمه البحث",
                      //    hintText: "بحث"
                    ),
                    //enabled: false,

                    // onChanged: (value) => _coponController.searchCopons(value),
                  ),
                  SizedBox(
                    height: height / 15,
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 10,
                      ),
                      Text("اختار البلد:"),
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 5,
                      ),
                      DropdownButton(
                        value: categoryProviderModel.selectedCity.nameAr,
                        items: _dropDownMenuItems,
                        onChanged: (c) {
                          print(c + " name " + categoryProviderModel.selectedCity.nameAr + " name");
                          setState(() {
                            for (int i = 0; i < cities.length; i++) {
                              cities[i].isSelected = false;
                            }
                            for (int i = 0; i < cities.length; i++) {
                              if (c == cities[i].nameAr) {
                                _currentCity = cities[i];
                                _currentCity.isSelected = true;
                              }
                            }
                          });
                        },
                      ),
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 30),
                      child: RaisedButton(
                          child: Padding(
                            padding: EdgeInsets.only(left: 30, right: 30),
                            child: Text("بحث",
                                style: new TextStyle(
                                    fontSize:
                                        20.0 //, fontFamily: FixedAssets.mainFont
                                    )),
                          ),
                          textColor: Colors.white,
                          color: Colors.lightBlue,
                          onPressed: () {
                            //AddToSchedule(context);
                            setState(() {
                              print(searchControlPopup.text + " words");
                              widget._categoryController.searchWord =
                                  searchControlPopup.text;
                              widget._categoryController.selectedCity =
                                  _currentCity;
                              widget._categoryController.cities = cities;
                              widget.refreshView();
                              widget._categoryController.getSearchValue();
                              Navigator.pop(context);
                            });
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0))))
                ],
              ),
            ),
          ),
          // ),
        ),
      ),
    );
  }
}
