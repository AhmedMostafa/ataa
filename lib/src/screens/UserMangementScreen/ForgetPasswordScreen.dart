import 'dart:convert';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class ForgetPasswordScreen extends StatefulWidget {
  @override
  ForgetPasswordScreenState createState() => ForgetPasswordScreenState();
}

class ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  TextEditingController emailController = new TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Color.fromRGBO(37, 97, 135, 1),

        body: Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(
            "assets/imgs/login_bg.png",
          ),
          fit: BoxFit.cover,
        ),
      ),
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new IconButton(
                          icon: new Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.2,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * .07,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/imgs/logo.png',
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text(
                      "نسيت الرقم السري",
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: SizedBox(
                      width: 50,
                      child: Divider(
                        color: Colors.white,
                        height: 15,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 16.0, horizontal: 30.0),
                    child: TextFormField(
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                      maxLines: 1, controller: emailController,
                      // autofocus: true,
//                      onSaved: (val) {
//                        _userController.model.userName = val;
//                      },
                      // controller: txtUserName,
                      onFieldSubmitted: (value) {
                        forgetPassword(context);
                      },
                      validator: validateEmail,
                      //textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        labelStyle:
                            new TextStyle(color: Colors.white, fontSize: 20.0),
                        labelText: "اسم المستخدم",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        padding: EdgeInsets.only(
                            top: 8.0, bottom: 8.0, right: 40.0, left: 40.0),
                        shape: StadiumBorder(
                            side: BorderSide(color: Colors.white)),
                        child: Text(
                          "ارسال",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Color.fromRGBO(1, 114, 132, 1),
                        onPressed: () {
                          forgetPassword(context);

                          // Navigator.pushReplacementNamed(context, "/Home");
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }

  String validateEmail(String val) {
    if (val.isEmpty)
      return "ادخل البريد الالكترونى";
    else {
      final _emailRegExpString = r'[a-zA-Z0-9\+\.\_\%\-\+]{1,256}\@[a-zA-Z0-9]'
          r'[a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})+';
      if (!RegExp(_emailRegExpString, caseSensitive: false).hasMatch(val)) {
        return "البريد الاكترونى غير صالح";
      } else
        return null;
    }
  }

  forgetPassword(context) async {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(child: CircularProgressIndicator());
        },
      );
      var client = new http.Client();
      client
          .get(ApiRoutes.restPassword + emailController.text)
          .timeout(Duration(seconds: 10))
          .then((data) {
        Navigator.pop(context);

        if (data.statusCode == 200) {
          Map response = json.decode(data.body);
          print(response.toString());
          Fluttertoast.showToast(
              msg: response.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          Fluttertoast.showToast(
              msg: "يوجد مشكله في الانترنت",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      });
    }
  }
}
