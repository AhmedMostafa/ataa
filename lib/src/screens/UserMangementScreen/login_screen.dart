import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/UserProviderModel.dart';
import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:atta_mobile_app/src/helper/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  bool mustLogin;

  LoginScreen(this.mustLogin);

  @protected
  @override
  createState() => LoginView();
}

class LoginView extends State<LoginScreen> {
 
  UserProviderModel _UserProviderModel;
  Future<bool> _willPopCallback() async {
    if (widget.mustLogin == true) {
      Navigator.pushReplacementNamed(context, "/Home");
    }
    return true;
  }

  FocusNode focusPasswordField = new FocusNode();
  GlobalKey<FormState> loginformKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> loginScaffoldkey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Consumer<UserProviderModel>(

      builder: (context, userProviderModel, child) =>Scaffold(
        

        key: loginScaffoldkey,
        body: OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            return new Stack(
              fit: StackFit.expand,
              children: [
                Center(
                    child: connected
                        ? WillPopScope(
                            onWillPop: _willPopCallback,
                            child: Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                      "assets/imgs/login_bg.png",
                                    ),
                                    // alignment: Alignment.topCenter,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                padding: EdgeInsets.symmetric(vertical: 15),
                                child: ListView(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Form(
                                        key: loginformKey,
                                        autovalidate:
                                            HomeProviderModel.loginAutoValid,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            widget.mustLogin == true
                                                ? Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      IconButton(
                                                          icon: Icon(
                                                            Icons.arrow_back_ios,
                                                            color: Colors.white,
                                                          ),
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context);
                                                          })
                                                    ],
                                                  )
                                                : Container(),
                                            SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  .07,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Image.asset(
                                                  'assets/imgs/logo.png',
                                                  height: 100,
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 10.0),
                                              child: Text(
                                                "مرحبا بعودتك",
                                                style: TextStyle(
                                                    fontSize: 25,
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 10.0),
                                              child: SizedBox(
                                                width: 50,
                                                child: Divider(
                                                  color: Colors.white,
                                                  height: 15,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 16.0,
                                                  horizontal: 30.0),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.white),
                                                maxLines: 1,
                                                //  autofocus: true,
                                                keyboardType:
                                                    TextInputType.emailAddress,

                                                onSaved: (val) {userProviderModel.model.userName=val;
                                                
                                                },
                                                // controller: txtUserName,
                                                onFieldSubmitted: (value) {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          focusPasswordField);
                                                },
                                                validator: (val) =>
                                                   userProviderModel.model
                                                        .validateEmail(val),
                                                textInputAction:
                                                    TextInputAction.next,
                                                decoration: InputDecoration(
                                                  labelStyle: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20.0),
                                                  labelText: "البريد الالكترونى",
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 16.0,
                                                  horizontal: 30.0),
                                              child: TextFormField(
                                                textAlign: TextAlign.end,
                                                textDirection: TextDirection.ltr,
                                                obscureText: true,
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.white),
                                                onSaved: (val) {
                                                  userProviderModel.model.password =
                                                      val;
                                                },
                                                maxLines: 1,
                                                focusNode: focusPasswordField,
                                                validator: (val) =>
                                                   userProviderModel.model
                                                        .validatePassword(val),
                                                textInputAction:
                                                    TextInputAction.go,
                                                onFieldSubmitted: (value) {
                                                 userProviderModel.signInWithEmailAndPassword(
                                                          context,
                                                          loginScaffoldkey,
                                                          loginformKey);
                                                },
                                                decoration: InputDecoration(
                                                  labelStyle: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20.0),
                                                  labelText: "الرقم السرى",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),

                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 30.0),
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    GestureDetector(
                                                      onTap: () {
                                                        Navigator.pushNamed(
                                                            context,
                                                            "/ForgetPassword");
                                                      },
                                                      child: Text(
                                                        "نسيت الرقم السرى؟",
                                                        style: TextStyle(
                                                            color: Colors.white),
                                                      ),
                                                    )
                                                  ]),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                RaisedButton(
                                                  padding: EdgeInsets.only(
                                                      top: 8.0,
                                                      bottom: 8.0,
                                                      right: 40.0,
                                                      left: 40.0),
                                                  shape: StadiumBorder(
                                                      side: BorderSide(
                                                          color: Colors.white)),
                                                  child: Text(
                                                    "دخول",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  color: Color.fromRGBO(
                                                      1, 114, 132, 1),
                                                  onPressed: () {
                                                  userProviderModel.signInWithEmailAndPassword(
                                                            context,
                                                            loginScaffoldkey,
                                                            loginformKey);

                                                    // Navigator.pushReplacementNamed(context, "/Home");
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                RaisedButton(
                                                  padding: EdgeInsets.only(
                                                      top: 8.0,
                                                      bottom: 8.0,
                                                      right: 40,
                                                      left: 40),
                                                  shape: StadiumBorder(),
                                                  child: Text(
                                                    "تسجيل",
                                                    style: TextStyle(
                                                        color: Colors.blue),
                                                  ),
                                                  color: Colors.white,
                                                  onPressed: () {
                                                  userProviderModel.navigateToRegPage(
                                                            context);
                                                  },
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            widget.mustLogin == false
                                                ? Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.center,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      RaisedButton(
                                                        padding: EdgeInsets.only(
                                                            top: 8.0,
                                                            bottom: 8.0,
                                                            right: 40.0,
                                                            left: 40.0),
                                                        shape: StadiumBorder(
                                                            side: BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        child: Text(
                                                          "تسجيل الدخول كضيف",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                        color: Color.fromRGBO(
                                                            1, 114, 132, 1),
                                                        onPressed: () {
                                                          Navigator.pushNamed(
                                                              context, "/Home");
                                                        },
                                                      ),
                                                    ],
                                                  )
                                                : Container(),
                                         
                                            SizedBox(
                                              height: 15,
                                            ),
                                      
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Scaffold(
                            body: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Opacity(
                                      opacity: 0.6,
                                      child: Image.asset(
                                        FixedAssets.noWifi,
                                        scale: 3,
                                      )),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  Text(
                                    'يرجي الاتصال بالإنترنت لعرض المحتوى..',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 19,
                                        color: HexColor("#ED949494"),
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          )),
              ],
            );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                'There are no bottons to push :)',
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
