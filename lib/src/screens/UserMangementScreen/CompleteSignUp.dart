import 'dart:convert';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/data/services/MemberInfoService.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class CompleteSignUp extends StatefulWidget {
  @override
  CompleteSignUpState createState() => new CompleteSignUpState();
}

class CompleteSignUpState extends State<CompleteSignUp> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController codeIDController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(
            "assets/imgs/login_bg.png",
          ),
          fit: BoxFit.cover,
        ),
      ),
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * .07,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/imgs/logo.png',
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Text(
                      "تكملة التسجيل",
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: SizedBox(
                      width: 50,
                      child: Divider(
                        color: Colors.white,
                        height: 15,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 16.0, horizontal: 30.0),
                    child: TextFormField(
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                      maxLines: 1,
                      //  autofocus: true,
                      keyboardType: TextInputType.phone,

                      onSaved: (val) {
                        phoneController.text = val;
                      },
                      // controller: txtUserName,

                      validator: validatePhone,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        labelStyle:
                            new TextStyle(color: Colors.white, fontSize: 20.0),
                        labelText: "رقم الهاتف",
                      ),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 16.0, horizontal: 30.0),
                    child: TextFormField(
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                      onSaved: (val) {
                        codeIDController.text = val;
                      },
                      maxLines: 1,
//                      focusNode: TextFieldController.focusPasswordField,
                      validator: validateStudentCode,
                      textInputAction: TextInputAction.go,
                      onFieldSubmitted: (value) {
                        // _userController.signInWithEmailAndPassword(context);
                        _submit(context);
                      },
                      decoration: InputDecoration(
                        labelStyle:
                            new TextStyle(color: Colors.white, fontSize: 20.0),
                        labelText: "كود الجامعي",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        padding: EdgeInsets.only(
                            top: 8.0, bottom: 8.0, right: 40.0, left: 40.0),
                        shape: StadiumBorder(
                            side: BorderSide(color: Colors.white)),
                        child: Text(
                          "تسجيل",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Color.fromRGBO(1, 114, 132, 1),
                        onPressed: () {
                          print(HomeProviderModel.userModel
                                  .toJson()
                                  .toString()
                                  .toString() +
                              " name");
                          _submit(context);
                          // _userController.signInWithEmailAndPassword(context);

                          // Navigator.pushReplacementNamed(context, "/Home");
                        },
                      ),
                      SizedBox(
                        width: 5,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }

  void _submit(context) async {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(child: CircularProgressIndicator());
        },
      );
      HomeProviderModel.userMemberModel.member.telephone =
          phoneController.text;
      HomeProviderModel.userMemberModel.member.activationCode =
          codeIDController.text;
      print(
          HomeProviderModel.userMemberModel.toJson().toString() + " donees");
      print(HomeProviderModel.token);
      await http
          .put(
              ApiRoutes.updateProfile +
                  phoneController.text.toString() +
                  "/" +
                  codeIDController.text.toString(),
              headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
              body: json.encode(HomeProviderModel.userMemberModel.toJson()))
          .then((response) {
        print(response.statusCode.toString());
        print(response.body.toString());

        if (response.statusCode == 204 ||
            response.statusCode == 201 ||
            response.statusCode == 200) {
          HomeProviderModel.loginAutoValid = false;
          HomeProviderModel.isGust = false;
          MemberInfoService().getAllMemberInfo();
          Navigator.pushNamedAndRemoveUntil(
              context, "/Home", (Route<dynamic> route) => false);
        }
      });
    }
  }

  String validatePhone(String val) {
    if (val.trim().isEmpty) {
      return "من فضلك ادخل رقم الهاتف";
    }

    Pattern pattern = r'(^[0-9]*$)';

    RegExp regExp = new RegExp(pattern);

    if (regExp.hasMatch(val.trim())) {
      // So, the email is valid
      return null;
    } else
      return "من فضلك ادخل رقم الهاتف صحيح";
  }

  String validateStudentCode(String val) {
    if (val.trim().isEmpty)
      return "من فضلك ادخل كود الجامعي";
    else
      return null;
  }
}
