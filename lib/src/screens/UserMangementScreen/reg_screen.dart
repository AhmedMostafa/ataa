import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/UserProviderModel.dart';
import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class RegScreen extends StatefulWidget {
  @protected
  @override
  createState() => RegView();
}

class RegView extends State<RegScreen> {
  
  UserProviderModel _UserProviderModel;

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProviderModel>(

      builder: (context,userProviderModel, child) =>
       Scaffold(
        key: HomeProviderModel.regScaffoldkey,
        backgroundColor: Color.fromRGBO(1, 114, 132, 1),
        body: Column(
          children: <Widget>[
            topBar(context),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Form(
                      key: HomeProviderModel.regformKey,
                      autovalidate: HomeProviderModel.regAutoValid,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              maxLines: 1,
                              autofocus: true,

                              onSaved: (val) {
                                userProviderModel.model.nameEn = val;
                              },
                              // controller: txtUserName,
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(
                                    HomeProviderModel.focusPhoneField);
                              },
                              validator: (val) =>
                                  userProviderModel.model.validateUserName(val),
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "اسم ",
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              maxLines: 1,

                              onSaved: (val) {
                                userProviderModel.model.telephone = val;
                              },
                              // controller: txtUserName,
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(
                                    HomeProviderModel.focusCodeField);
                              },
                              focusNode: HomeProviderModel.focusPhoneField,
                              validator: (val) =>
                                  userProviderModel.model.validatePhone(val),
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "رقم الهاتف",
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              maxLines: 1,

                              onSaved: (val) {
                                userProviderModel.model.studentCode = val;
                              },
                              // controller: txtUserName,
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(
                                    HomeProviderModel.focusEmailField);
                              },
                              focusNode: HomeProviderModel.focusCodeField,
                              validator: (val) =>
                                  userProviderModel.model.validateStudentCode(val),
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "كود الجامعي",
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              maxLines: 1,
                              focusNode: HomeProviderModel.focusEmailField,
                              onSaved: (val) {
                                userProviderModel.model.email = val;
                                userProviderModel.model.userName = val;
                              },
                              // controller: txtUserName,
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(
                                    HomeProviderModel.focusRegPasswordField);
                              },
                              validator: (val) =>
                                  userProviderModel.model.validateEmail(val),
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "البريد الاكترونى",
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              textAlign: TextAlign.end,
                              textDirection: TextDirection.ltr,
                              enableInteractiveSelection: false,
                              obscureText: true,
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              onSaved: (val) {
                                userProviderModel.model.password = val;
                              },
                              maxLines: 1,
                              controller: HomeProviderModel.passwordController,
                              focusNode:
                                  HomeProviderModel.focusRegPasswordField,
                              onFieldSubmitted: (value) {
                                FocusScope.of(context).requestFocus(
                                    HomeProviderModel.focusRetypePasswordField);
                              },
                              validator: (val) =>
                                  userProviderModel.model.validatePassword(val),
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "الرقم السرى",
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 30.0),
                            child: TextFormField(
                              textAlign: TextAlign.end,
                              textDirection: TextDirection.ltr,
                              enableInteractiveSelection: false,
                              obscureText: true,
                              style:
                                  TextStyle(fontSize: 16.0, color: Colors.white),
                              maxLines: 1,
                              focusNode:
                                  HomeProviderModel.focusRetypePasswordField,
                              validator: (val) => userProviderModel.model
                                  .validateConfirmPassword(val),
                              textInputAction: TextInputAction.go,
                              onFieldSubmitted: (value) {
                                userProviderModel.regNewUser(context);
                              },
                              decoration: InputDecoration(
                                labelStyle: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                labelText: "تاكيد الرقم السرى",
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              RaisedButton(
                                padding: EdgeInsets.only(
                                    top: 8.0, bottom: 8.0, right: 40, left: 40),
                                shape: StadiumBorder(),
                                child: Text(
                                  "تسجيل",
                                  style: TextStyle(color: Colors.blue),
                                ),
                                color: Colors.white,
                                onPressed: () {
                                  userProviderModel.regNewUser(context);
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget topBar(BuildContext context) {
    return Material(
      type: MaterialType.canvas,
      color: Colors.white,
      elevation: 2,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(10),
        bottomRight: Radius.circular(10),
      ),
      child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[myAppBar(context)],
          )),
    );
  }

  Widget myAppBar(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 2),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new IconButton(
                    icon: new Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xffFF4E00),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.25,
                ),
                Text(
                  "تسجيل جديد",
                  style: TextStyle(fontSize: 18, color: FixedAssets.mainColor
                      //Color(0xff793B8E),
                      ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
