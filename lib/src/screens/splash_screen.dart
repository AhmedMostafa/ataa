import 'dart:convert';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/data/models/TokenModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';
import 'package:atta_mobile_app/src/data/services/MemberInfoService.dart';
import 'package:atta_mobile_app/src/data/services/UserService.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SplashScreen extends StatefulWidget {
  @protected
  @override
  createState() => SplashView();
}

class SplashView extends State<SplashScreen> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  loadTokenFromCache() async {
    await SharedPreference.getUserToken().then((data) async {
      print("value " + data.toString());
      Token mytoken = new Token();
      if (data != null) {
        var jsonmodel = json.decode(data);
        mytoken = Token.fromJson(jsonmodel);

        setState(() {
          print("vvv " + mytoken.token);
          HomeProviderModel.token = mytoken.token;
        });
        await checkToken();
      } else {
        Navigator.pushReplacementNamed(context, "/Login");
      }
    }, onError: (error) {
      Navigator.pushReplacementNamed(context, "/Login");
    });
  }

  checkToken() async {
    print(ApiRoutes.checkToken + " check token");
    print("vaa " + HomeProviderModel.token.toString());
    await http.post(
        //TextFieldController.apiUrl + "auth/checkToken"
        ApiRoutes.checkToken,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "bearer ${HomeProviderModel.token}"
        }).then((response) async {
      print(response.statusCode.toString() + "code");
      if (response.statusCode == 200) {
        HomeProviderModel.isGust = false;
        MemberInfoService().getAllMemberInfo();
        Navigator.pushReplacementNamed(context, "/Home");
      } else {
        await SharedPreference.removeUserToken();
        await renewToken();
      }
    }).catchError((error) async {
      await SharedPreference.removeUserToken();
      await renewToken();
    });
  }

  renewToken() async {
    UserModel userModel = await SharedPreference.getUser();
    UserService _userService = new UserService();
    http.Response isValidCredentials =
        await _userService.signInWithEmailAndPassword(userModel);
    if (isValidCredentials != null) {
      MemberInfoService().getAllMemberInfo();
      HomeProviderModel.isGust = false;
      Navigator.pushReplacementNamed(context, "/Home");
    } else {
      Navigator.pushReplacementNamed(context, "/Login");
    }
  }

  Future onSelectNotification(String payload) async {
    print(payload.toString() + " ttt");
//    Navigator.push(
//      context,
//      new MaterialPageRoute(
//        builder: (context) => SearchView(),
//      ),
//    );
//    showDialog(
//      context: context,
//      builder: (_) {
//        return new AlertDialog(
//          title: Text("PayLoad"),
//          content: Text("Payload : $payload"),
//        );
//      },
//    );
  }

  @override
  void initState() {
    loadTokenFromCache();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> msg) async {
        //if (msg != null) {
        print("done");
        //}
        print((msg["data"].toString() + " onMessagecalled"));
        print((msg.toString() + " onMessagecalled"));
      },
      onLaunch: (Map<String, dynamic> msg) async {
        print("done");
        print((msg["data"].toString() + " onMessagecalled"));
        print(("onLaunch called"));
      },
      onResume: (Map<String, dynamic> msg) async {
        //if (msg != null) {
        print("done");
        //}
        print((msg["data"].toString() + " onMessagecalled"));
        print(("onResume called"));
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(alert: true, badge: true, sound: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print(" iso setting Registered");
    });

    _firebaseMessaging.getToken().then((fff) {
      HomeProviderModel.firebaseToken = fff;
      print(fff.toString() + " firebase token");
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        'assets/imgs/splash.png',
        fit: BoxFit.cover,
      ),
    ));
  }
}
