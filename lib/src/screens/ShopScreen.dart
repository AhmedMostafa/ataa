// import 'dart:convert';
// import 'dart:io';

// import 'package:atta_mobile_app/src/Utils/FixedAssets.dart';
// import 'package:atta_mobile_app/src/controllers/ShopController.dart';
// import 'package:atta_mobile_app/src/data/SharedPreference.dart';
// import 'package:atta_mobile_app/src/data/models/MemberInfoModel.dart';
// import 'package:atta_mobile_app/src/data/models/ShopModel.dart';
// import 'package:atta_mobile_app/src/data/services/ShopService.dart';
// import 'package:atta_mobile_app/src/helper/HexColor.dart';
// import 'package:atta_mobile_app/src/screens/branch_location.dart';
// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_rating/flutter_rating.dart';
// import 'package:http/http.dart' as http;
// import 'package:mvc_pattern/mvc_pattern.dart';
// import 'package:open_file/open_file.dart';
// import 'package:url_launcher/url_launcher.dart';

// import '../TextFiledController.dart';

// // import 'package:atta_mobile_app/src/widgets/SharedWidget.dart';
// // import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
// // import 'package:path_provider/path_provider.dart';

// // import 'PDFViwerScreen.dart';

// class ShopScreen extends StatefulWidget {
//   ShopService shopService;
//   ShopModel shopModel;

//   List<NetworkImage> sliderimages = new List<NetworkImage>();

//   ShopScreen(this.shopModel) {
//     sliderimages.clear();
//     if (shopModel != null && shopModel.shopPics != null) {
//       for (int i = 0; i < shopModel.shopPics.length; i++) {
//         sliderimages.add(new NetworkImage(shopModel.shopPics[i]["path"]));
//       }
//     }
//     if (sliderimages.length == 0) {
//       sliderimages.add(
//           new NetworkImage("https://www.w3schools.com/w3css/img_forest.jpg"));
//     }
//   }
//   @override
//   ShopScreenState createState() => ShopScreenState();
// }

// class ShopScreenState extends StateMVC<ShopScreen> {
//   ShopScreenState() : super(ShopController()) {
//     _shopController = ShopController.con;
//   }
//   ShopController _shopController;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Column(
//         children: <Widget>[
//           topBar(context),
//           Expanded(
//             child: ListView(
//               children: <Widget>[
//                 card(context),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Container(
//                       width: MediaQuery.of(context).size.width * 0.75,
//                       child: Text(
//                         widget.shopModel.descriptionAr != null
//                             ? widget.shopModel.descriptionAr
//                             : "",
//                         style: TextStyle(
//                             fontSize: 17,
//                             fontWeight: FontWeight.w500,
//                             color: HexColor("#A2A2A2")),
//                       ),
//                     )
//                   ],
//                 ),
//                 SizedBox(
//                   height: 15,
//                 ),
//                 Container(
//                   height: 150,
//                   child: Padding(
//                     padding: const EdgeInsets.all(10.0),
//                     child: Card(
//                       color: Colors.white.withOpacity(0.9),
//                       elevation: 2.0,
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.only(
//                               bottomRight: Radius.circular(10),
//                               topRight: Radius.circular(10),
//                               topLeft: Radius.circular(25),
//                               bottomLeft: Radius.circular(25))),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           SizedBox(
//                             width: 5,
//                           ),
//                           Column(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             children: <Widget>[
//                               Row(
//                                 children: <Widget>[
//                                   Text(
//                                     "كوبون  قابل للاستعمال اكثر من ",
//                                     style: TextStyle(
//                                         color: Colors.green, fontSize: 12),
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),

//                           SizedBox(
//                             width: 5,
//                           ),
//                           InkWell(
//                             child: Container(
//                               height: 90,
//                               child: Card(
//                                   child: Padding(
//                                     padding: const EdgeInsets.all(5.0),
//                                     child: Column(
//                                       children: <Widget>[
//                                         Image.asset(
//                                           FixedAssets.disc,
//                                           fit: BoxFit.cover,
//                                           // scale: 5,
//                                         ),
//                                         SizedBox(
//                                             width: 90,
//                                             child: Text(
//                                               "اﻟﺮﺟﺎء اﻟﻀﻐﻂ ﻟﻠﺤﺼﻮل على الكوبون",
//                                               textAlign: TextAlign.center,
//                                               style: TextStyle(fontSize: 10),
//                                             ))
//                                       ],
//                                     ),
//                                   ),
//                                   color: Colors.white,
//                                   elevation: 2.0,
//                                   shape: RoundedRectangleBorder(
//                                       borderRadius: BorderRadius.all(
//                                           Radius.circular(6)))),
//                             ),
//                             onTap: () {
//                               return showDialog(
//                                   context: context,
//                                   barrierDismissible: true,
//                                   builder: (BuildContext context) {
//                                     bool _isLoading = false;
//                                     return Dialog(
//                                       shape: RoundedRectangleBorder(
//                                         borderRadius: BorderRadius.circular(10),
//                                       ),
//                                       child: Container(
//                                           height: MediaQuery.of(context)
//                                                   .size
//                                                   .height *
//                                               0.3,
//                                           width: MediaQuery.of(context)
//                                                   .size
//                                                   .width *
//                                               0.3,
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(20.0),
//                                           ),
//                                           child: Column(
//                                             children: <Widget>[
//                                               Stack(
//                                                 children: <Widget>[
//                                                   Container(
//                                                     height:
//                                                         MediaQuery.of(context)
//                                                                 .size
//                                                                 .height *
//                                                             0.3,
//                                                   ),
//                                                   Container(
//                                                     height:
//                                                         MediaQuery.of(context)
//                                                                 .size
//                                                                 .height *
//                                                             0.1,
//                                                     decoration: BoxDecoration(
//                                                         borderRadius:
//                                                             BorderRadius.only(
//                                                                 topLeft: Radius
//                                                                     .circular(
//                                                                         10.0),
//                                                                 topRight: Radius
//                                                                     .circular(
//                                                                         10.0)),
//                                                         color:
//                                                             Color(0xffFF4E00)),
//                                                   ),
//                                                   // Center(
//                                                   //   child: Padding(
//                                                   //     padding: const EdgeInsets
//                                                   //             .symmetric(
//                                                   //         horizontal: 90.0,
//                                                   //         vertical: 90.0),
//                                                   //     child: Text(
//                                                   //       widget.shopModel
//                                                   //           .contactEmail,
//                                                   //       style: TextStyle(
//                                                   //           color: Colors.black,
//                                                   //           fontWeight:
//                                                   //               FontWeight.w500,
//                                                   //           fontSize: 18),
//                                                   //     ),
//                                                   //   ),
//                                                   // ),
//                                                   Positioned(
//                                                     top: 120.0,
//                                                     right: 165.0,
//                                                     child: _isLoading
//                                                         ? CircularProgressIndicator()
//                                                         : FlatButton(
//                                                             color: Color(
//                                                                 0xffFF4E00),
//                                                             child: Text(
//                                                               "إلغاء",
//                                                               style: TextStyle(
//                                                                   color: Colors
//                                                                       .white,
//                                                                   fontWeight:
//                                                                       FontWeight
//                                                                           .w500),
//                                                             ),
//                                                             onPressed: () {
//                                                               Navigator.pop(
//                                                                   context);
//                                                             },
//                                                           ),
//                                                   ),
//                                                   Positioned(
//                                                     top: 120.0,
//                                                     right: 20.0,
//                                                     child: _isLoading
//                                                         ? CircularProgressIndicator()
//                                                         : FlatButton(
//                                                             color: Color(
//                                                                 0xffFF4E00),
//                                                             child: Text(
//                                                               "تأكيد",
//                                                               style: TextStyle(
//                                                                   color: Colors
//                                                                       .white,
//                                                                   fontWeight:
//                                                                       FontWeight
//                                                                           .w500),
//                                                             ),
//                                                             onPressed:
//                                                                 () async {
//                                                               setState(() {
//                                                                 _isLoading =
//                                                                     true;
//                                                               });

//                                                               Navigator.pop(
//                                                                   context);
//                                                             },
//                                                           ),
//                                                   ),
//                                                   Column(
//                                                     children: <Widget>[
//                                                       Padding(
//                                                         padding:
//                                                             const EdgeInsets
//                                                                 .all(15.0),
//                                                         child: Text(
// //                                                          "رقم الكوبون",
//                                                           "سيتم استخدام الكوبون هل تريد تأكيد العملية؟",
//                                                           style: TextStyle(
//                                                               fontSize: 18.0,
//                                                               color:
//                                                                   Colors.white,
//                                                               fontWeight:
//                                                                   FontWeight
//                                                                       .w600),
//                                                         ),
//                                                       ),
//                                                     ],
//                                                   ),
//                                                 ],
//                                               )
//                                             ],
//                                           )),
//                                     );
//                                   });
//                             },
//                           ),

//                           // RaisedButton(
//                           //   shape: RoundedRectangleBorder(
//                           //     borderRadius:
//                           //         BorderRadius.all(Radius.circular(5.0)),
//                           //   ),
//                           //   color: Colors.white,
//                           // )
//                           SizedBox(
//                             width: 15,
//                           ),
//                           Container(
//                             decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.only(
//                                     topLeft: Radius.circular(15.0),
//                                     bottomLeft: Radius.circular(15.0)),
//                                 color: HexColor("CC6C4B")),
//                             height: double.infinity,
//                             width: 25,
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   height: 15,
//                 ),
//                 widget.shopModel.shopItems.length != 0
//                     ? Padding(
//                         padding: const EdgeInsets.only(
//                             top: 5.0, right: 15, left: 15),
//                         child: Row(
//                           children: <Widget>[
//                             Image.asset('assets/imgs/list_lows.png'),
//                             SizedBox(
//                               width: 5,
//                             ),
//                             Text(
//                               "قائمه الخدمات",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   color: Color(0xffe8440c)),
//                             )
//                           ],
//                         ),
//                       )
//                     : Container(),
//                 widget.shopModel.shopItems != null
//                     ? ListView.builder(
//                         shrinkWrap: true,
//                         physics: ClampingScrollPhysics(),
//                         itemCount: widget.shopModel.shopItems == null
//                             ? 0
//                             : widget.shopModel.shopItems.length,
//                         itemBuilder: (context, index) {
//                           return Padding(
//                             padding: const EdgeInsets.only(right: 15.0, top: 5),
//                             child: Column(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: <Widget>[
//                                 Row(
//                                   mainAxisAlignment: MainAxisAlignment.start,
//                                   children: <Widget>[
//                                     Image.asset('assets/imgs/list_item.png'),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     Text(
//                                       "الاسم:  ",
//                                       style: TextStyle(fontSize: 12),
//                                     ),
//                                     Text(
//                                       widget.shopModel.shopItems[index]
//                                                   .nameAr !=
//                                               null
//                                           ? widget
//                                               .shopModel.shopItems[index].nameAr
//                                           : "",
//                                       style: TextStyle(color: Colors.grey),
//                                     )
//                                   ],
//                                 ),
//                                 Row(
//                                   children: <Widget>[
//                                     SizedBox(
//                                       width: 30,
//                                     ),
//                                     Text(
//                                       "السعر قبل : ",
//                                       style:
//                                           TextStyle(color: Color(0xffe8440c)),
//                                     ),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     Text(
//                                       widget.shopModel.shopItems[index]
//                                           .priceBefore
//                                           .toString(),
//                                       style: TextStyle(color: Colors.grey),
//                                     )
//                                   ],
//                                 ),
//                                 Row(
//                                   children: <Widget>[
//                                     SizedBox(
//                                       width: 30,
//                                     ),
//                                     Text(
//                                       "السعر بعد : ",
//                                       style:
//                                           TextStyle(color: Color(0xffe8440c)),
//                                     ),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     Text(
//                                       widget
//                                           .shopModel.shopItems[index].priceAfter
//                                           .toString(),
//                                       style: TextStyle(color: Colors.grey),
//                                     )
//                                   ],
//                                 )
//                               ],
//                             ),
//                           );
//                         },
//                       )
//                     : SizedBox(
//                         height: 5,
//                       ),
//                 SizedBox(
//                   height: 15,
//                 ),
//                 widget.shopModel.shopItems.length != 0
//                     ? Padding(
//                         padding: const EdgeInsets.only(
//                             top: 5.0, right: 15, left: 15),
//                         child: Row(
//                           children: <Widget>[
//                             Image.asset('assets/imgs/list_advantage.png'),
//                             SizedBox(
//                               width: 5,
//                             ),
//                             Text(
//                               "المميزات",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   color: Color(0xffe8440c)),
//                             )
//                           ],
//                         ),
//                       )
//                     : Container(),
//                 ListView.builder(
//                   shrinkWrap: true,
//                   physics: ClampingScrollPhysics(),
//                   itemCount: widget.shopModel.shopBenefits == null
//                       ? 0
//                       : widget.shopModel.shopBenefits.length,
//                   itemBuilder: (context, index) {
//                     return Padding(
//                       padding: const EdgeInsets.only(right: 15.0, top: 5),
//                       child: Row(
//                         children: <Widget>[
//                           Image.asset('assets/imgs/list_item.png'),
//                           SizedBox(
//                             width: 5,
//                           ),
//                           SizedBox(
//                             width: 300,
//                             child: Text(
//                               widget.shopModel.shopBenefits[index].descriptionAr
//                                   .toString(),
//                               style:
//                                   TextStyle(color: Colors.grey, fontSize: 15),
//                             ),
//                           )
//                         ],
//                       ),
//                     );
//                   },
//                 ),
//                 SizedBox(
//                   height: 15,
//                 ),
//                 widget.shopModel.shopTerms.length != 0
//                     ? Padding(
//                         padding: const EdgeInsets.only(
//                             top: 5.0, right: 15, left: 15),
//                         child: Row(
//                           children: <Widget>[
//                             Image.asset('assets/imgs/list_lows.png'),
//                             SizedBox(
//                               width: 5,
//                             ),
//                             Text(
//                               "شروط و أحكام",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   color: Color(0xffe8440c)),
//                             )
//                           ],
//                         ),
//                       )
//                     : Container(),
//                 ListView.builder(
//                   shrinkWrap: true,
//                   physics: ClampingScrollPhysics(),
//                   itemCount: widget.shopModel.shopTerms == null
//                       ? 0
//                       : widget.shopModel.shopTerms.length,
//                   itemBuilder: (context, index) {
//                     return Padding(
//                       padding: const EdgeInsets.only(right: 15.0, top: 5),
//                       child: Row(
//                         children: <Widget>[
//                           Image.asset('assets/imgs/list_item.png'),
//                           SizedBox(
//                             width: 5,
//                           ),
//                           SizedBox(
//                             width: 300,
//                             child: Text(
//                               widget.shopModel.shopTerms[index].descriptionAr,
//                               style:
//                                   TextStyle(color: Colors.grey, fontSize: 15),
//                             ),
//                           )
//                         ],
//                       ),
//                     );
//                   },
//                 ),
//                 SizedBox(
//                   height: 30,
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//       //endDrawer: MyDrawer(10, this.context),
//     );
//   }

//   Widget topBar(BuildContext context) {
//     return Material(
//       type: MaterialType.canvas,
//       color: Colors.white,
//       elevation: 3,
//       borderRadius: BorderRadius.only(
//         bottomLeft: Radius.circular(10),
//         bottomRight: Radius.circular(10),
//       ),
//       child: Padding(
//           padding:
//               const EdgeInsets.only(top: 10.0, left: 5, right: 5, bottom: 20),
//           child: Column(
//             children: <Widget>[myAppBar(context)],
//           )),
//     );
//   }

//   Widget myAppBar(
//     BuildContext context,
//   ) {
//     return SafeArea(
//       child: Padding(
//         padding: EdgeInsets.only(top: 5, bottom: 2),
//         child: Column(
//           children: <Widget>[
//             Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: <Widget>[
//                 new IconButton(
//                     icon: new Icon(
//                       Icons.arrow_back_ios,
//                       color: Color(0xffFF4E00),
//                     ),
//                     onPressed: () {
//                       Navigator.pop(context);
//                     }),
//                 SizedBox(
//                   width: MediaQuery.of(context).size.width * 0.3,
//                 ),
//                 Container(
//                   child: Row(
//                     children: <Widget>[
//                       Text(
//                         "تفاصيل",
//                         style: TextStyle(
//                           fontWeight: FontWeight.bold,
//                           fontSize: 20,
//                           color: Color(0xffFF4E00),
//                         ),
//                       ),
//                       //Icon(Icons.chat_bubble_outline,color: Color(0xffFF4E00),size: 15,)
//                     ],
//                   ),
//                 ),
// //                Builder(
// //                  builder: (context) => IconButton(
// //                      icon: new Icon(
// //                        Icons.menu,
// //                        color: Color(0xffFF4E00),
// //                      ),
// //                      onPressed: () {
// //                        Scaffold.of(context).openEndDrawer();
// //                      }),
// //                ),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   Widget carousel() => new Carousel(
//         boxFit: BoxFit.cover,
//         showIndicator: false,
//         images: widget.sliderimages,
//         animationCurve: Curves.fastOutSlowIn,
//         animationDuration: Duration(seconds: 2),
//       );

//   Widget card(
//     BuildContext context,
//   ) =>
//       Container(
//         padding: EdgeInsets.all(8),
//         //height: 250,
//         child: Card(
//           color: HexColor("#F9F9F9"),
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(8.0))),
//           child: Column(
//             children: <Widget>[
//               Container(
//                   height: 150,
//                   child: ClipRRect(
//                     borderRadius: BorderRadius.only(
//                       topLeft: Radius.circular(8),
//                       topRight: Radius.circular(8),
//                     ),
//                     child: carousel(),
//                     // Image.asset(
//                     //   'assets/imgs/2.jpg',
//                     //   fit: BoxFit.fitWidth,
//                     //   width: MediaQuery.of(context).size.width,
//                     // )
//                   )),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   Expanded(
//                     child: Padding(
//                       padding: const EdgeInsets.only(
//                           top: 12.0, left: 8, right: 8, bottom: 8),
//                       child: Column(
//                         children: <Widget>[
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             children: <Widget>[
//                               Container(
//                                 width: MediaQuery.of(context).size.width * .35,
//                                 child: Column(
//                                   children: <Widget>[
//                                     Text(
//                                         widget.shopModel == null ||
//                                                 widget.shopModel.nameAr == null
//                                             ? ""
//                                             : widget.shopModel.nameAr,
//                                         style: TextStyle(
//                                             fontWeight: FontWeight.w900,
//                                             color: Color(0xffe8440c),
//                                             fontSize: 19)),
//                                     StarRating(
//                                       size: MediaQuery.of(context).size.width *
//                                           0.35 /
//                                           7,
//                                       rating: widget.shopModel == null ||
//                                               widget.shopModel.shopRate == null
//                                           ? 0.5
//                                           : widget.shopModel.shopRate,
//                                       color: Colors.orange,
//                                       borderColor: Colors.grey,
//                                       starCount: 5,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               // Container(
//                               //   width: MediaQuery.of(context).size.width * .10,
//                               //   child: Column(
//                               //     children: <Widget>[
//                               //       Text(
//                               //           _offerDetailController.offer.discountAmount == null
//                               //               ? ""
//                               //               : "الخصم " +
//                               //                   _offerDetailController.offer.discountAmount +
//                               //                   "%",
//                               //           style: TextStyle(
//                               //               fontWeight: FontWeight.bold,
//                               //               color: Color(0xffe8440c),
//                               //               fontSize: 12)),
//                               //       Text(
//                               //           _offerDetailController.offer.shop == null ||
//                               //                   _offerDetailController.offer.shop.addressAr ==
//                               //                       null
//                               //               ? ""
//                               //               : _offerDetailController.offer.shop.addressAr,
//                               //           style: TextStyle(
//                               //               color: Colors.grey, fontSize: 12)),
//                               //     ],
//                               //   ),
//                               // ),
//                               //SizedBox(width: 5,),
//                               Column(
//                                 children: <Widget>[
//                                   widget.shopModel.discountAmount == "" ||
//                                           widget.shopModel.discountAmount
//                                               .toString()
//                                               .isEmpty
//                                       ? Container()
//                                       : Text(
//                                           " خصم ${widget.shopModel.discountAmount}",
//                                           style: TextStyle(
//                                               color: Color(0xffe8440c),
//                                               fontSize: 22.0,
//                                               fontWeight: FontWeight.w700),
//                                         ),
//                                   SizedBox(
//                                     height: 24,
//                                   )
//                                 ],
//                               ),

//                               FlatButton(
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/imgs/location-pin.png',
//                                       color: widget.shopModel.shopBranches
//                                                   .length !=
//                                               0
//                                           ? Color(0xffe8440c)
//                                           : Colors.grey,
//                                     ),
//                                     Text("الموقع", //  "الفروع المتاحه",
//                                         style: TextStyle(
//                                             color: Colors.grey, fontSize: 18))
//                                   ],
//                                 ),
//                                 onPressed: () {
//                                   // print("lengthlengthlengthlength" +
//                                   //     widget.shopModel.shopBranches.length
//                                   //         .toString());
//                                   // if (widget.shopModel.shopBranches.length != 0)
//                                     // Navigator.push(
//                                     //     context,
//                                     //     MaterialPageRoute(
//                                     //         builder: (BuildContext context) =>
//                                     //             BranchLocationScreen(widget
//                                     //                 .shopModel.shopBranches))
                                                    
//                                     //                 );
//                                 },
//                               )
//                             ],
//                           ),
//                           SizedBox(height: 15),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: <Widget>[
//                               Container(
//                                   child: Card(
//                                 shape: StadiumBorder(),
//                                 color: Color(0xff5ec80e),
//                                 child: Padding(
//                                   padding: const EdgeInsets.only(
//                                       right: 5.0, left: 5.0),
//                                   child: Row(
//                                     children: <Widget>[
//                                       Text(
//                                         widget.shopModel == null ||
//                                                 widget.shopModel.shopRate ==
//                                                     null
//                                             ? "0.5"
//                                             : widget.shopModel.shopRate
//                                                 .toString(),
//                                         style: TextStyle(color: Colors.white),
//                                       ),
//                                       Icon(
//                                         Icons.star,
//                                         color: Colors.white,
//                                         size: 17,
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               )),
//                               IconButton(
//                                 icon: Icon(
//                                   Icons.call,
//                                   color: Colors.green,
//                                 ),
//                                 onPressed: () {
//                                   launchURLPhone();
//                                 },
//                               ),
//                               IconButton(
//                                 icon: Icon(
//                                   Icons.email,
//                                   color: Colors.red,
//                                 ),
//                                 onPressed: () {
//                                   launchURLEmail();
//                                 },
//                               ),
//                               widget.shopModel.offersFilePath != null
//                                   ? IconButton(
//                                       icon: Icon(
//                                         Icons.attach_file,
//                                         color: Colors.blueGrey,
//                                       ),
//                                       onPressed: () {
//                                         // LiquidCircularProgressIndicatorPage();
//                                         _shopController.loadPdf(
//                                             widget.shopModel, context);
//                                       },
//                                     )
//                                   : Container(),
//                               widget.shopModel == null
//                                   ? Container()
//                                   : Container(
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.end,
//                                         children: <Widget>[
//                                           SizedBox(
//                                             width: 5,
//                                           ),
//                                           IconButton(
//                                             icon: Icon(Icons.favorite),
//                                             iconSize: 20,
//                                             color:
//                                                 widget.shopModel.isFavorite ==
//                                                         true
//                                                     ? Colors.red
//                                                     : Colors.grey,
//                                             onPressed: () {
//                                               setState(() {
//                                                 if (widget
//                                                         .shopModel.isFavorite ==
//                                                     true) {
//                                                   widget.shopModel.isFavorite =
//                                                       false;
//                                                   deleteFavoritShop(
//                                                       widget.shopModel.shopId);
//                                                 } else {
//                                                   widget.shopModel.isFavorite =
//                                                       true;
//                                                   saveFavoritShop(
//                                                       widget.shopModel.shopId);
//                                                 }
//                                               });
//                                             },
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                             ],
//                           ),
//                         ],
//                       ),
//                     ),
//                   )
//                 ],
//               )
//             ],
//           ),
//         ),
//       );

//   launchURLPhone() async {
//     if (widget.shopModel.contactTele != null) {
//       var url = "tel:${widget.shopModel.contactTele}";
//       if (await canLaunch(url)) {
//         await launch(url);
//       } else {
//         throw 'Could not launch $url';
//       }
//     }
//   }

//   launchURLEmail() async {
//     if (widget.shopModel.contactEmail != null) {
//       var url = "mailto:${widget.shopModel.contactEmail}";
//       if (await canLaunch(url)) {
//         await launch(url);
//       } else {
//         throw 'Could not launch $url';
//       }
//     }
//   }

//   Future loadDocument(BuildContext context) async {
//     // showDialog(
//     //     barrierDismissible: false,
//     //     context: context,
//     //     builder: (context) {
//     //       return SharedWidget.loading(context);
//     //     });

//     File file;
//     file = await widget.shopService.getFileFromUrl(
//         widget.shopModel.offersFilePath, widget.shopModel.shopId.toString());
//     // Navigator.pop(context);
//     OpenFile.open(file.path);
//   }
//   // document = await PDFDocument.fromURL(widget.shopModel.offersFilePath);
// //    document = await PDFDocument.fromURL(
// //        "http://icube-icps.unistra.fr/img_auth.php/d/db/ModernC.pdf");
// //    PDFViewer(document: document);
//   // Navigator.push(context,
//   //     MaterialPageRoute(builder: (context) => PDFViewScreen(document)));
// //    setState(() => _isLoading = false);

//   // Future<File> getFileFromUrl(String url, String dataId) async {
//   //   // check if file exists local
//   //   var dir = await getApplicationDocumentsDirectory();
//   //   File localFile = File("${dir.path}/$dataId.pdf");
//   //   bool isFileExists = await localFile.exists();
//   //   if (isFileExists) {
//   //     return localFile;
//   //   } else {
//   //     var data = await http
//   //         .get("http://icube-icps.unistra.fr/img_auth.php/d/db/ModernC.pdf");
//   //     var bytes = data.bodyBytes;
//   //     File file = File("${dir.path}/$dataId.pdf");

//   //     File urlFile = await file.writeAsBytes(bytes);
//   //     return urlFile;
//   //   }
//   // }

//   Future<bool> saveFavoritShop(int shopId) async {
//     MemberInfoModel memberInfoModel = await SharedPreference.getMemberInfo();
//     var model = {
//       "MemberId": memberInfoModel.memberId.toString(),
//       "ShopId": shopId.toString()
//     };
//     return await http.post(
//       TextFieldController.apiUrl + "FavoriteShop",
//       body: json.encode(model),
//       headers: {
//         'content-type': 'application/json',
//         'Authorization': 'bearer ${TextFieldController.token}'
//       },
//     ).then((response) {
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         return true;
//       } else
//         return false;
//     });
//   }

//   Future<bool> deleteFavoritShop(int shopId) async {
//     MemberInfoModel memberInfoModel = await SharedPreference.getMemberInfo();
//     return await http.delete(
//       TextFieldController.apiUrl +
//           "FavoriteShop/${shopId.toString()}/${memberInfoModel.memberId.toString()}",
//       headers: {'Authorization': 'bearer ${TextFieldController.token}'},
//     ).then((response) {
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         return true;
//       } else
//         return false;
//     });
//   }
// }
