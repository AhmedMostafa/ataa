import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/LayoutProviderModel.dart';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/models/CategoryModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';
import 'package:atta_mobile_app/src/data/services/MemberInfoService.dart';
import 'package:atta_mobile_app/src/helper/HexColor.dart';
import 'package:atta_mobile_app/src/screens/category_screen.dart';
import 'package:atta_mobile_app/src/widgets/Animation.dart';
import 'package:atta_mobile_app/src/widgets/BannerSlider.dart';
import 'package:atta_mobile_app/src/widgets/HomeSlider.dart';
import 'package:atta_mobile_app/src/widgets/SharedWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatefulWidget {
  @protected
  @override
  createState() => HomeView();
}

class HomeView extends State<HomeScreen> {

  MemberInfoService infoService = new MemberInfoService();
  LayoutProviderModel _layoutProviderModel = LayoutProviderModel();
  UserModel userModel;
 RefreshController _refreshController =
      RefreshController(initialRefresh: false);
       void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
bool isLoaded = false;
didChangeDependencies() {
  super.didChangeDependencies();
  if(isLoaded == false){
   Provider.of<HomeProviderModel>(context).init(context);

   isLoaded=true;
  }
 
}

  @override
  Widget build(BuildContext context) {
    return   Consumer<HomeProviderModel>(

      builder: (context, homeProviderModel, child) => Scaffold(
        // endDrawer: MyDrawer(1, this.context),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          actions: <Widget>[
            Builder(
              builder: (context) => IconButton(
                  icon: new Icon(
                    HomeProviderModel.isGust
                        ? Icons.arrow_forward_ios
                        : Icons.menu,
                    color: Color(0xffFF4E00),
                  ),
                  onPressed: () {
                    HomeProviderModel.isGust
                        ? Navigator.pop(context)
                        : Scaffold.of(this.context).openEndDrawer();
                  }),
            ),
         
          ],
          centerTitle: true,
          title: Text(
            "عطاء",
            style: TextStyle(color: FixedAssets.mainColor, fontSize: 19),
          ),
          backgroundColor: Colors.white,
          elevation: 1.0,
        ),
        //body: categoryGirdView(),
        body: OfflineBuilder(
          connectivityBuilder: (BuildContext context,
              ConnectivityResult connectivity, Widget child) {
            final bool connected = connectivity != ConnectivityResult.none;
            return  Stack(
               fit: StackFit.expand,
               children: [
                 SmartRefresher(
            enablePullDown: true,
                                enablePullUp: true,
               header: ClassicHeader(
                 idleText: "",
                 releaseText: "",
                 completeText: "",
               ),
            
               controller: _refreshController,
               onRefresh: _onRefresh,
                   child: Center(
                       child: connected
                           ? homeProviderModel.allCategoryLoading
                               ? SharedWidget.loading(context)
                               :    SmartRefresher(
            enablePullDown: true,
                                enablePullUp: true,
               header: ClassicHeader(
                 idleText: "",
                 releaseText: "",
                 completeText: "",
               ),
            
               controller: _refreshController,
               onRefresh: _onRefresh,child: Container(
                                   child: ListView(
                                     physics: ClampingScrollPhysics(),
                                     padding: EdgeInsets.zero,
                                     shrinkWrap: true,
                                     children: <Widget>[
                                       Material(
                                         type: MaterialType.canvas,
                                         color: Colors.white,
                                         elevation: 2,
                                         child: Padding(
                                             padding: const EdgeInsets.all(0),
                                             child: Column(
                                               children: <Widget>[
                                                 Padding(
                                                   padding:
                                                       const EdgeInsets.all(16.0),
                                                   child: categoryView(),
                                                 )
                                               ],
                                             )),
                                       ),
                                       SizedBox(
                                         height: 10,
                                       ),
                                       homeProviderModel.listBanner.length != 0
                                           ? FadeIn(
                                               0.8,
                                               BannerSlider(
                                                 "لافتة اعلانات",
                                                 homeProviderModel.listBanner,
                                               ))
                                           : SizedBox(
                                               height: 2,
                                             ),
                                       homeProviderModel.latestOffer.length != 0
                                           ? FadeIn(
                                               0.8,
                                               HomeSlider(
                                                   "أخر العروض",
                                                   homeProviderModel.latestOffer,
                                                   context))
                                           : SizedBox(
                                               height: 2,
                                             ),
                                       homeProviderModel.topOffer.length != 0
                                           ? FadeIn(
                                               1.2,
                                               HomeSlider(
                                                 "الأكثر تداولاً",
                                                 homeProviderModel.topOffer,
                                                 context,
                                                 isSlow: true,
                                               ))
                                           : SizedBox(
                                               height: 2,
                                             ),
                                     ],
                                   ),
                                ) )
                           : Scaffold(
                               body: Center(
                                 child: Column(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                   children: <Widget>[
                                     Opacity(
                                         opacity: 0.6,
                                         child: Image.asset(
                                           FixedAssets.noWifi,
                                           scale: 3,
                                         )),
                                     SizedBox(
                                       height: 25,
                                     ),
                                     Text(
                                       'يرجي الاتصال بالإنترنت لعرض المحتوى..',
                                       textAlign: TextAlign.center,
                                       style: TextStyle(
                                           fontSize: 19,
                                           color: HexColor("#ED949494"),
                                           fontWeight: FontWeight.w300),
                                     ),
                                   ],
                                 ),
                               ),
                             )),
                 ),
               ],
             );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                'There are no bottons to push :)',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget topBar(BuildContext context) {
    return Center(
      child: Material(
        type: MaterialType.canvas,
        color: Colors.white,
        elevation: 2,
        child: Padding(
            padding: const EdgeInsets.all(0),
            child: Column(
              children: <Widget>[
                myAppBar(context),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: categoryView(),
                )
              ],
            )),
      ),
    );
  }

  Widget myAppBar(BuildContext context) {
    return SafeArea(
      child: Padding(
          padding: EdgeInsets.only(top: 5, bottom: 2),
//        child: Column(
//          children: <Widget>[
          child: Card(
            child: SizedBox(
              width: MediaQuery.of(context).size.width + 300,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
              
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .2,
                  ),
                  Text(
                    "عطاء",
                    style:
                        TextStyle(color: FixedAssets.mainColor, fontSize: 19),
                  ),
                ],
              ),
            ),
          )
//          ],
//        ),
          ),
    );
  }

  Widget categoryView() {
   
    return   Consumer<HomeProviderModel>(

      builder: (context, homeProviderModel, child) =>
    
       FadeIn(
          0.3,
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "الفئات",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Color(0xff793B8E),
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                categoryViewHorizontal( homeProviderModel.allCategory),
              
              ],
            ),
          )),
    );
  }
  

  Widget getCategory() {
    List<Widget> rows = <Widget>[];
    int itemsNum = 0;
    if (Provider.of<HomeProviderModel>(context).viewAllCategory) {
     Provider.of<HomeProviderModel>(context).allCategory.length;
    } else {
      itemsNum = Provider.of<HomeProviderModel>(context).allCategory.length > 3
          ? 3
          :Provider.of<HomeProviderModel>(context).allCategory.length;
    }
    int rowCount = (itemsNum / 4).ceil();
    for (int i = 0; i < rowCount; i++) {
      List<Widget> col = <Widget>[];
      if (!Provider.of<HomeProviderModel>(context).viewAllCategory &&
        Provider.of<HomeProviderModel>(context).allCategory.length > 3) {
        col.add(viewAllcategory("عرض الكل"));
      } else if( Provider.of<HomeProviderModel>(context).viewAllCategory && i == rowCount - 1) {
        col.add(viewAllcategory("عرض جزء"));
      }

      for (var j = 0; j < 4; j++) {
        int x = j + i * 4;
        if (x < itemsNum) col.add(category(Provider.of<HomeProviderModel>(context).allCategory[x]));
      }

      rows.add(Row(children: col));
    }

    return Column(
      children: rows,
    );
  }

  Widget category(CategoryModel ) {
    return  Consumer<HomeProviderModel>(

      builder: (context, todos, child) =>
      Expanded(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            padding: EdgeInsets.all(0),
            child: Card(
              color: todos.categoryModel.color == null ? Color(0xff793B8E) : HexColor(todos.categoryModel.color),
              shape: CircleBorder(),
              child: Padding(
                  padding: const EdgeInsets.all(13.0),
                  child: todos.categoryModel.img == null
                      ? Icon(
                          Icons.category,
                          color: Colors.white,
                        )
                      : Image.network(
                          todos.categoryModel.img,
                          height: 27,
                          width: 27,
                        )),
            ),
            onPressed: () {
              HomeProviderModel.categoryID = todos.categoryModel.categoryId;
              HomeProviderModel.appBar = todos.categoryModel.nameAr;
              _layoutProviderModel.setCurrentTab(1);
            },
          ),
          Text(
            todos.categoryModel.nameAr,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 20.0,
          )
        ],
      )),
    );
  }

  Widget viewAllcategory(String name) {
    return  Consumer<HomeProviderModel>(

      builder: (context, todos, child) =>
       Expanded(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FlatButton(
            padding: EdgeInsets.all(0),
            child: Card(
              color: Colors.white,
              shape: CircleBorder(),
              child: Padding(
                padding: const EdgeInsets.all(13.0),
                child:
                    // Image.asset(
                    //   "assets/imgs/right-chevron.png",
                    //   height: 27,
                    //   width: 27,
                    // )
                    Icon(
                  Icons.arrow_back_ios,
                  color: Color(0xff793B8E),
                  size: 27.0,
                ),
              ),
            ),
            onPressed: () {
            todos.viewCategory();
            },
          ),
          Text(
            name,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 20.0,
          )
        ],
      )),
    );
  }

  Widget categoryViewHorizontal(List<CategoryModel> cats) {
    return 
     Container(
        height: MediaQuery.of(context).size.height * 0.15,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount:cats.length,
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, i) {
            return FadeIn(
                i.toDouble() * 0.4,
                Column(
                  children: <Widget>[
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      child: Card(
                        color:cats[i].color == null
                            ? Color(0xff793B8E)
                            : HexColor(cats[i].color),
                        shape: CircleBorder(),
                        child: Padding(
                            padding: const EdgeInsets.all(13.0),
                            child: cats[i].img == null
                                ? Icon(
                                    Icons.category,
                                    color: Colors.white,
                                  )
                                : Image.network(
                                     cats[i].img,
                                    height: 27,
                                    width: 27,
                                  )),
                      ),
                      onPressed: () {
                        if (HomeProviderModel.isGust == false) {
                       HomeProviderModel.categoryID = cats[i].categoryId;
                       HomeProviderModel.appBar = cats[i].nameAr;
                       _layoutProviderModel.setCurrentTab(1);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CategoryScreen(
                                    cats[i].nameAr, cats[i].categoryId)));
                        } else {
                          HomeProviderModel.mustLoginView(context);
                        }
                      },
                    ),
                    Text(
                     cats[i].nameAr,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 20.0,
                    )
                  ],
                ));
          },
        ),
     
    );
  }
}
