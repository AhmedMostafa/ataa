
import 'package:atta_mobile_app/src/ProviderModel/CategoryProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/ProviderModel/LayoutProviderModel.dart';
import 'package:atta_mobile_app/src/widgets/Animation.dart';
import 'package:atta_mobile_app/src/widgets/SearchCategoryPopup.dart';
import 'package:atta_mobile_app/src/widgets/SharedWidget.dart';
import 'package:atta_mobile_app/src/widgets/companyCard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';




class CategoryScreen extends StatefulWidget {
  
  String categoryName;
  int categoryID;

  CategoryScreen(this.categoryName, this.categoryID);

  @protected
  @override
  _CategoryView createState() => _CategoryView();
}

class _CategoryView extends State<CategoryScreen> {
  double anim = 1.0;

  TextEditingController searchControl = TextEditingController();


  CategoryProviderModel _categoryController;
  LayoutProviderModel _LayoutProviderModel = LayoutProviderModel();

bool isLoaded = false;
didChangeDependencies() {
  super.didChangeDependencies();
  if(isLoaded == false){
   Provider.of<CategoryProviderModel>(context).categoryID = widget.categoryID;
   Provider.of<CategoryProviderModel>(context).searchWord =searchControl.text;
      Provider.of<CategoryProviderModel>(context).getCategoryByID(widget.categoryID);
       Provider.of<CategoryProviderModel>(context).getCities();
       Provider.of<CategoryProviderModel>(context).init();
HomeProviderModel.noSearch = false;
   isLoaded=true;
  }
 
}


  @override
  void dispose() {
     Provider.of<CategoryProviderModel>(context).clearData();
    super.dispose();
  }

  refreshView() {
    searchControl.text = Provider.of<CategoryProviderModel>(context).searchWord;
    HomeProviderModel.noSearch = true;
    // refresh();
  }

  @override
  Widget build(BuildContext context) {
    return  Consumer<CategoryProviderModel>(

      builder: (context, modelCategoryProviderModel, child) =>
      Scaffold(
        body: modelCategoryProviderModel.isloading
            ?
//      Expanded(
//              // child: SharedWidget.loading(context),
//              child:
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SharedWidget.noResult(noResult: 2),
                  SizedBox(
                    height: 20,
                  ),
                  SharedWidget.loading(context)
                ],
              )
//    )
            : Container(
                child: WillPopScope(
                  onWillPop: () {
                    Navigator.pop(context);
                    _LayoutProviderModel.setCurrentTab(0);
                    return null;
                  },
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        topBar(context),
                        searchCard(),
                        modelCategoryProviderModel.categories.length != 0
                            ? Expanded(
                                child: ListView.builder(
                                  itemCount:
                                      modelCategoryProviderModel.categories.length,
                                  itemBuilder: (context, index) {
                                    return FadeIn(
                                        //index.toDouble() * 1.0,
                                        3.0,
                                        CompanyCard(
                                          shopModel: modelCategoryProviderModel
                                              .categories[index],
                                          favScreen: false,
                                        ));
                                  },
                                ),
                              )
                            : SharedWidget.noResult(noResult: 1),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  Widget searchCard() => Padding(
      padding:
          const EdgeInsets.only(top: 5, left: 8.0, right: 8.0, bottom: 0.0),
      child: Container(
        child: Card(
          color: Color(0xffF1F2F3),
          elevation: 1.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 8.0, right: 8.0, top: 2.0, bottom: 2.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.search,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      searchControl.text = Provider.of<CategoryProviderModel>(context).searchWord;
                      // refresh();
                      print(Provider.of<CategoryProviderModel>(context).cities.length.toString() +
                          "length of cities");
                      // showDialog(
                      //     context: context,
                      //     builder: (BuildContext context) {
                      //       return SearchCategoryPopup(
                      //          _categoryController, refreshView);
                      //     });
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: IgnorePointer(
                        ignoring: true,
                        child: TextField(
                          textInputAction: TextInputAction.search,
                          controller: searchControl,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: "بحث"),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ));

  Widget topBar(BuildContext context) {
    return Material(
      type: MaterialType.canvas,
      color: Colors.white,
      elevation: 3,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(10),
        bottomRight: Radius.circular(10),
      ),
      child: Padding(
          padding:
              const EdgeInsets.only(top: 10.0, left: 5, right: 5, bottom: 20),
          child: Column(
            children: <Widget>[myAppBar(context)],
          )),
    );
  }

  Widget myAppBar(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 2),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new IconButton(
                    icon: new Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xffFF4E00),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
//                      _LayoutProviderModel.setCurrentTab(0);
                    }),
                SizedBox(width: MediaQuery.of(context).size.width * .32),

                Text(
                  widget.categoryName,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Color(0xffFF4E00),
                  ),
                ),
                //Icon(Icons.chat_bubble_outline,color: Color(0xffFF4E00),size: 15,)

//                new IconButton(
//                    icon: new Icon(
//                      Icons.menu,
//                      color: Color(0xffFF4E00),
//                    ),
//                    onPressed: () {
//                      TextFieldController.layoutScaffoldkey.currentState
//                          .openEndDrawer();
//                    }),
              ],
            )
          ],
        ),
      ),
    );
  }
}
