// To parse this JSON data, do
//
//     final memperInfoModel = memperInfoModelFromJson(jsonString);

import 'dart:convert';

MemberInfoModel memberInfoModelFromJson(String str) {
  final jsonData = json.decode(str);
  return MemberInfoModel.fromJson(jsonData);
}

String memberInfoModelToJson(MemberInfoModel data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class MemberInfoModel {
  int memberId;
  String nameAr;
  String nameEn;
  String email;
  String telephone;
  String profilePic;
  String activationCode;

  MemberInfoModel(
      {this.memberId,
      this.nameAr,
      this.nameEn,
      this.email,
      this.telephone,
      this.profilePic,
      this.activationCode});

  factory MemberInfoModel.fromJson(Map<String, dynamic> json) =>
      new MemberInfoModel(
          memberId: json["memberId"] == null ? null : json["memberId"],
          nameAr: json["nameAr"],
          nameEn: json["nameEn"],
          email: json["email"] == null ? null : json["email"],
          telephone: json["telephone"],
          profilePic: json["profilePic"] == null ? null : json["profilePic"],
          activationCode: json["activationCode"]);

  Map<String, dynamic> toJson() => {
        "memberId": memberId == null ? null : memberId,
        "nameAr": nameAr,
        "nameEn": nameEn,
        "email": email == null ? null : email,
        "telephone": telephone,
        "profilePic": profilePic == null ? null : profilePic,
      };
}
