// To parse this JSON data, do
//
//     final shopModel = shopModelFromJson(jsonString);

import 'package:atta_mobile_app/src/data/models/ShopBranchModel.dart';

//List<ShopModel> shopModelFromJson(String str) => new List<ShopModel>.from(
//    json.decode(str).map((x) => ShopModel.fromJson(x)));
//
//String shopModelToJson(List<ShopModel> data) =>
//    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class ShopModel {
  int shopId;
  String nameAr;
  String nameEn;
  bool isActive;
  String contactEmail;
  String contactTele;
  String profilPic;
  String logoPic;
  String descriptionAr;
  String descriptionEn;
  int categoryId;
  String userId;
  String openingHoursFrom;
  String openingHoursTo;
  bool is24Hours;
  String addressAr;
  String addressEn;
  dynamic category;
  dynamic discountAmount;
  double shopRate;
  bool isFavorite = false;
  bool isFavoriteLoading = false;

  dynamic user;
  List<dynamic> favoriteShops;
  List<dynamic> offers;
  List<ShopBranchModel> shopBranches;
  List<dynamic> shopPics;
  List<dynamic> shopRates;

  String offersFilePath;
  List<ShopItem> shopItems;
  List<Shop> shopTerms;
  List<Shop> shopBenefits;
  ShopModel(
      {this.shopId,
      this.nameAr,
      this.nameEn,
      this.isActive,
      this.contactEmail,
      this.contactTele,
      this.profilPic,
      this.logoPic,
      this.descriptionAr,
      this.descriptionEn,
      this.categoryId,
      this.userId,
      this.openingHoursFrom,
      this.openingHoursTo,
      this.is24Hours,
      this.addressAr,
      this.addressEn,
      this.category,
      this.shopRate,
      this.isFavorite = false,
      this.isFavoriteLoading = false,
      this.user,
      this.favoriteShops,
      this.offers,
      this.shopBranches,
      this.shopPics,
      this.shopRates,
      this.offersFilePath,
      this.shopItems,
      this.shopTerms,
      this.shopBenefits,
      this.discountAmount});

  factory ShopModel.fromJson(Map<String, dynamic> json) {
    return new ShopModel(
      shopId: json["shopId"] == null ? null : json["shopId"],
      nameAr: json["nameAr"] == null ? null : json["nameAr"],
      nameEn: json["nameEn"] == null ? null : json["nameEn"],
      discountAmount:
          json["discountAmount"] == null ? null : json["discountAmount"],
      isActive: json["isActive"] == null ? null : json["isActive"],
      contactEmail: json["contactEmail"] == null ? null : json["contactEmail"],
      contactTele: json["contactTele"] == null ? null : json["contactTele"],
      profilPic: json["profilPic"] == null ? null : json["profilPic"],
      logoPic: json["logoPic"] == null ? null : json["logoPic"],
      descriptionAr:
          json["descriptionAr"] == null ? null : json["descriptionAr"],
      descriptionEn:
          json["descriptionEn"] == null ? null : json["descriptionEn"],
      categoryId: json["categoryId"] == null ? null : json["categoryId"],
      userId: json["userId"] == null ? null : json["userId"],
      openingHoursFrom:
          json["openingHoursFrom"] == null ? null : json["openingHoursFrom"],
      openingHoursTo:
          json["openingHoursTo"] == null ? null : json["openingHoursTo"],
      is24Hours: json["is24Hours"] == null ? null : json["is24Hours"],
      addressAr: json["addressAr"] == null ? null : json["addressAr"],
      addressEn: json["addressEn"] == null ? null : json["addressEn"],
      category: json["category"],
      shopRate: json["shopRate"] == null ? null : json["shopRate"],
      offersFilePath:
          json["offersFilePath"] == null ? null : json["offersFilePath"],
      isFavorite: json["isFavorite"] == null ? false : json["isFavorite"],
      user: json["user"],
      favoriteShops: json["favoriteShops"] == null
          ? null
          : new List<dynamic>.from(json["favoriteShops"].map((x) => x)),
      offers: json["offers"] == null
          ? null
          : new List<dynamic>.from(json["offers"].map((x) => x)),
      shopBranches: json["shopBranches"] == null
          ? null
          : new List<ShopBranchModel>.from(
              json["shopBranches"].map((x) => ShopBranchModel.fromJson(x))),
      shopPics: json["shopPics"] == null
          ? null
          : new List<dynamic>.from(json["shopPics"].map((x) => x)),
      shopRates: json["shopRates"] == null
          ? null
          : new List<dynamic>.from(json["shopRates"].map((x) => x)),
      shopItems: new List<ShopItem>.from(
          json["shopItems"].map((x) => ShopItem.fromJson(x))),
      shopTerms:
          new List<Shop>.from(json["shopTerms"].map((x) => Shop.fromJson(x))),
      shopBenefits: new List<Shop>.from(
          json["shopBenefits"].map((x) => Shop.fromJson(x))),
    );
  }
}

class ShopItem {
  int shopItemId;
  String nameAr;
  String nameEn;
  double discount;
  String descriptionAr;
  String descriptionEn;
  int shopId;
  double priceBefore;
  double priceAfter;
  ShopModel shop;

  ShopItem({
    this.shopItemId,
    this.nameAr,
    this.nameEn,
    this.discount,
    this.descriptionAr,
    this.descriptionEn,
    this.shopId,
    this.priceBefore,
    this.priceAfter,
    this.shop,
  });

  factory ShopItem.fromJson(Map<String, dynamic> json) => new ShopItem(
        shopItemId: json["shopItemId"],
        nameAr: json["nameAr"],
        nameEn: json["nameEn"],
        discount: json["discount"],
        descriptionAr: json["descriptionAr"],
        descriptionEn: json["descriptionEn"],
        shopId: json["shopId"],
        priceBefore: json["priceBefore"],
        priceAfter: json["priceAfter"],
        shop: json["shop"],
      );
}

class Shop {
  int shopBenefitId;
  int shopId;
  String descriptionAr;
  String descriptionEn;
  dynamic shop;

  int shopTermId;

  Shop({
    this.shopBenefitId,
    this.shopId,
    this.descriptionAr,
    this.descriptionEn,
    this.shop,
    this.shopTermId,
  });

  factory Shop.fromJson(Map<String, dynamic> json) => new Shop(
        shopBenefitId:
            json["shopBenefitId"] == null ? null : json["shopBenefitId"],
        shopId: json["shopId"],
        descriptionAr: json["descriptionAr"],
        descriptionEn: json["descriptionEn"],
        shop: json["shop"],
        shopTermId: json["shopTermId"] == null ? null : json["shopTermId"],
      );

  Map<String, dynamic> toJson() => {
        "shopBenefitId": shopBenefitId == null ? null : shopBenefitId,
        "shopId": shopId,
        "descriptionAr": descriptionAr,
        "descriptionEn": descriptionEn,
        "shop": shop,
        "shopTermId": shopTermId == null ? null : shopTermId,
      };
}
