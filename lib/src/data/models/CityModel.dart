class CityModel {
  int cityId;
  String nameAr;
  bool isSelected;

  CityModel({this.cityId, this.nameAr, this.isSelected});

  factory CityModel.fromJson(Map<String, dynamic> json) => new CityModel(
      cityId: json['cityId'], nameAr: json['nameAr'], isSelected: false);
}
