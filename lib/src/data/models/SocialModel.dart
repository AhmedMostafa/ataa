class SocialModel {
  String name, email, provider, image, providerId;

  SocialModel(
      this.name, this.email, this.provider, this.image, this.providerId);
}
