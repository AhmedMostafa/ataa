// To parse this JSON data, do
//
//     final bannerModel = bannerModelFromJson(jsonString);

import 'dart:convert';

List<BannerModel> bannerModelFromJson(String str) =>
    List<BannerModel>.from(json.decode(str).map((x) => BannerModel.fromMap(x)));

class BannerModel {
  int bannerId;
  String nameAr;
  String nameEn;
  String url;
  String img;

  BannerModel({
    this.bannerId,
    this.nameAr,
    this.nameEn,
    this.url,
    this.img,
  });

  factory BannerModel.fromMap(Map<String, dynamic> json) => BannerModel(
        bannerId: json["bannerId"] == null ? 0 : json["BannerId"],
        nameAr: json["nameAr"] == null ? "" : json["nameAr"],
        nameEn: json["nameEn"] == null ? "" : json["nameEn"],
        url: json["url"] == null ? "" : json["url"],
        img: json["img"] == null ? "" : json["img"],
      );
}
