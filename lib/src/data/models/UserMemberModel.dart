import 'dart:convert';

UserMemberModel userMemberModelFromJson(String str) =>
    UserMemberModel.fromJson(json.decode(str));

String userMemberModelToJson(UserMemberModel data) =>
    json.encode(data.toJson());

class UserMemberModel {
  Member member;

  UserMemberModel({
    this.member,
  });

  factory UserMemberModel.fromJson(Map<String, dynamic> json) =>
      new UserMemberModel(
        member: Member.fromJson(json["member"]),
      );

  Map<String, dynamic> toJson() => {
        "member": member.toJson(),
      };
}

class Member {
  int memberId;
  String nameAr;
  String nameEn;
  String email;
  String telephone;
  bool isActive;
  String activationCode;
  String profilePic;
  String userId;
  User user;
  List<dynamic> favoriteShops;
  List<dynamic> memberOffers;
  List<dynamic> shopRates;

  Member({
    this.memberId,
    this.nameAr,
    this.nameEn,
    this.email,
    this.telephone,
    this.isActive,
    this.activationCode,
    this.profilePic,
    this.userId,
    this.user,
    this.favoriteShops,
    this.memberOffers,
    this.shopRates,
  });

  factory Member.fromJson(Map<String, dynamic> json) => new Member(
        memberId: json["memberId"],
        nameAr: json["nameAr"],
        nameEn: json["nameEn"],
        email: json["email"],
        telephone: json["telephone"],
        isActive: json["isActive"],
        activationCode: json["activationCode"],
        profilePic: json["profilePic"],
        userId: json["userId"],
        user: json['user'] == null ? null : User.fromJson(json["user"]),
        favoriteShops:
            new List<dynamic>.from(json["favoriteShops"].map((x) => x)),
        memberOffers:
            new List<dynamic>.from(json["memberOffers"].map((x) => x)),
        shopRates: new List<dynamic>.from(json["shopRates"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "memberId": memberId,
        "nameAr": nameAr,
        "nameEn": nameEn,
        "email": email,
        "telephone": telephone,
        "isActive": isActive,
        "activationCode": activationCode,
        "profilePic": profilePic,
        "userId": userId,
        "favoriteShops": new List<dynamic>.from(favoriteShops.map((x) => x)),
        "memberOffers": new List<dynamic>.from(memberOffers.map((x) => x)),
        "shopRates": new List<dynamic>.from(shopRates.map((x) => x)),
      };
}

class User {
  String id;
  String userName;
  String normalizedUserName;
  String email;
  String normalizedEmail;
  bool emailConfirmed;
  String passwordHash;
  String securityStamp;
  String concurrencyStamp;
  String phoneNumber;
  bool phoneNumberConfirmed;
  bool twoFactorEnabled;
  String lockoutEnd;
  bool lockoutEnabled;
  int accessFailedCount;

  User({
    this.id,
    this.userName,
    this.normalizedUserName,
    this.email,
    this.normalizedEmail,
    this.emailConfirmed,
    this.passwordHash,
    this.securityStamp,
    this.concurrencyStamp,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.twoFactorEnabled,
    this.lockoutEnd,
    this.lockoutEnabled,
    this.accessFailedCount,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
        id: json["id"],
        userName: json["userName"],
        normalizedUserName: json["normalizedUserName"],
        email: json["email"],
        normalizedEmail: json["normalizedEmail"],
        emailConfirmed: json["emailConfirmed"],
        passwordHash: json["passwordHash"],
        securityStamp: json["securityStamp"],
        concurrencyStamp: json["concurrencyStamp"],
        phoneNumber: json["phoneNumber"],
        phoneNumberConfirmed: json["phoneNumberConfirmed"],
        twoFactorEnabled: json["twoFactorEnabled"],
        lockoutEnd: json["lockoutEnd"],
        lockoutEnabled: json["lockoutEnabled"],
        accessFailedCount: json["accessFailedCount"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "userName": userName,
        "normalizedUserName": normalizedUserName,
        "email": email,
        "normalizedEmail": normalizedEmail,
        "emailConfirmed": emailConfirmed,
        "passwordHash": passwordHash,
        "securityStamp": securityStamp,
        "concurrencyStamp": concurrencyStamp,
        "phoneNumber": phoneNumber,
        "phoneNumberConfirmed": phoneNumberConfirmed,
        "twoFactorEnabled": twoFactorEnabled,
        "lockoutEnd": lockoutEnd,
        "lockoutEnabled": lockoutEnabled,
        "accessFailedCount": accessFailedCount,
      };
}
