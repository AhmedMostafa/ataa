
import 'dart:convert';

import 'package:atta_mobile_app/src/data/models/UserMemberModel.dart';

class TrophyModel {
    int competitionId;
    String nameAr;
    String nameEn;
    String img;
    dynamic startDate;
    dynamic endDate;
    dynamic finalEnrollmentDate;
    String description;
    String brief;
    bool winnerSelected;
    bool canEnroll;
    bool isEnrolled;
    bool isWin;
    Member winner; 
    dynamic isDeleted;
    dynamic createdDate;
    dynamic modifiedDate;
    dynamic createdByNameId;
    dynamic modifiedByNameId;
    dynamic createdByName;
    dynamic modifiedByName;

    TrophyModel({
        this.competitionId,
        this.nameAr,
        this.nameEn,
        this.img,
        this.startDate,
        this.endDate,
        this.finalEnrollmentDate,
        this.description,
        this.brief,
        this.winnerSelected,
        this.canEnroll,
        this.isEnrolled,
        this.isWin,
        this.winner,
        this.isDeleted,
        this.createdDate,
        this.modifiedDate,
        this.createdByNameId,
        this.modifiedByNameId,
        this.createdByName,
        this.modifiedByName,
    });

    factory TrophyModel.fromJson(String str) => TrophyModel.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory TrophyModel.fromMap(Map<String, dynamic> json) => new TrophyModel(
        competitionId: json["competitionId"] == null ? null : json["competitionId"],
        nameAr: json["nameAr"] == null ? null : json["nameAr"],
        nameEn: json["nameEn"] == null ? null : json["nameEn"],
        img: json["img"] == null ? null : json["img"],
        startDate: json["startDate"] == null ? null : json["startDate"],
        endDate: json["endDate"] == null ? null : json["endDate"],
        finalEnrollmentDate: json["finalEnrollmentDate"] == null ? null : json["finalEnrollmentDate"],
        description: json["description"] == null ? null : json["description"],
        brief: json["brief"] == null ? null : json["brief"],
        winnerSelected: json["winnerSelected"] == null ? null : json["winnerSelected"],
        canEnroll: json["canEnroll"] == null ? null : json["canEnroll"],
        isEnrolled: json["isEnrolled"] == null ? null : json["isEnrolled"],
        isWin: json["isWin"] == null ? null : json["isWin"],
        winner:json["winner"]==null?new Member(): Member.fromJson(json["winner"]),
        isDeleted: json["isDeleted"],
        createdDate: json["createdDate"] == null ? null : json["createdDate"],
        modifiedDate: json["modifiedDate"],
        createdByNameId: json["createdByNameId"],
        modifiedByNameId: json["modifiedByNameId"],
        createdByName: json["createdByName"],
        modifiedByName: json["modifiedByName"],
    );

    Map<String, dynamic> toMap() => {
        "competitionId": competitionId == null ? null : competitionId,
        "nameAr": nameAr == null ? null : nameAr,
        "nameEn": nameEn == null ? null : nameEn,
        "img": img == null ? null : img,
        "startDate": startDate == null ? null : startDate.toIso8601String(),
        "endDate": endDate == null ? null : endDate.toIso8601String(),
        "finalEnrollmentDate": finalEnrollmentDate == null ? null : finalEnrollmentDate.toIso8601String(),
        "description": description == null ? null : description,
        "brief": brief == null ? null : brief,
        "winnerSelected": winnerSelected == null ? null : winnerSelected,
        "canEnroll": canEnroll == null ? null : canEnroll,
        "isEnrolled": isEnrolled == null ? null : isEnrolled,
        "isWin": isWin == null ? null : isWin,
        "winner": winner,
        "isDeleted": isDeleted,
        "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
        "modifiedDate": modifiedDate,
        "createdByNameId": createdByNameId,
        "modifiedByNameId": modifiedByNameId,
        "createdByName": createdByName,
        "modifiedByName": modifiedByName,
    };
}