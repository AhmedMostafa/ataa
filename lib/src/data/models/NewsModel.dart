// To parse this JSON data, do
//
//     final newsModel = newsModelFromJson(jsonString);

import 'dart:convert';

List<NewsModel> newsModelFromJson(String str) => new List<NewsModel>.from(
    json.decode(str).map((x) => NewsModel.fromJson(x)));

class NewsModel {
  int newsId;
  String titleAr;
  String titleEn;
  String descriptionAr;
  String descriptionEn;
  String briefAr;
  String briefEn;
  String imagePath;
  String formatedEventDate;
  bool isFavorite = false;
  bool isFavoriteLoading = false;

  NewsModel(
      {this.newsId,
      this.titleAr,
      this.titleEn,
      this.descriptionAr,
      this.descriptionEn,
      this.briefAr,
      this.briefEn,
      this.imagePath,
      this.formatedEventDate,
      this.isFavorite = false,
      this.isFavoriteLoading = false});

  factory NewsModel.fromJson(Map<String, dynamic> json) => new NewsModel(
        newsId: json["newsId"] == null ? null : json["newsId"],
        titleAr: json["titleAr"] == null ? null : json["titleAr"],
        titleEn: json["titleEn"] == null ? null : json["titleEn"],
        descriptionAr:
            json["descriptionAr"] == null ? null : json["descriptionAr"],
        descriptionEn:
            json["descriptionEn"] == null ? null : json["descriptionEn"],
        briefAr: json["briefAr"] == null ? null : json["briefAr"],
        briefEn: json["briefEn"] == null ? null : json["briefEn"],
        imagePath: json["imagePath"] == null ? null : json["imagePath"],
        formatedEventDate: json["formatedEventDate"] == null
            ? null
            : json["formatedEventDate"],
        isFavorite: json["isFavorite"] == null ? false : json["isFavorite"],
      );
}
