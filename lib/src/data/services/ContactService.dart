import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/Utils/api_routes.dart';


import 'package:http/http.dart' as http;

class ContactService {
  // @override
  // Future<ContactModel> getContact() async {
  //   return await http.get(ApiRoutes.contact + "/1", headers: {
  //     'Authorization': 'bearer ${TextFieldController.token}'
  //   }).then((response) {
  //     if (response.statusCode == 200) {
  //       var jsonModel = json.decode(response.body);
  //       ContactModel contactModel = ContactModel.fromJson(jsonModel);
  //       return contactModel;
  //     } else
  //       return new ContactModel();
  //   });
  // }

  @override
  Future<bool> sendContactMsg(String msg, String sub) async {
    var model = {
      "title": sub,
      "message": msg,
    };
    return await http
        .post(ApiRoutes.contact,
            headers: {
              'Authorization': 'bearer ${HomeProviderModel.token}',
              'content-type': 'application/json'
            },
            body: jsonEncode(model))
        .then((response) {
      if (response.statusCode == 200 || response.statusCode == 201) {
        return true;
      } else
        return false;
    }).catchError((error) {
      return false;
    });
  }
}
