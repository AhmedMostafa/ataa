import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/data/models/MemberInfoModel.dart';
import 'package:atta_mobile_app/src/data/models/UserMemberModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';

import 'package:http/http.dart' as http;

class MemberInfoService  {
  Future<MemberInfoModel> getAllMemberInfo() async {
    print("asdasdasd");
    UserModel user = await SharedPreference.getUser();
    print(user.userName);
    print(ApiRoutes.MemberInfoService + user.userName);
    print("vvvv " + HomeProviderModel.token.toString());
    return await http.get(
      //TextFieldController.apiUrl + "Member/MemberByUser/"
      ApiRoutes.MemberInfoService + user.userName,
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        print(jsonValue.toString() + " user info");
        MemberInfoModel memberInfoModel = MemberInfoModel.fromJson(jsonValue);
        HomeProviderModel.memberInfoModel =
            MemberInfoModel.fromJson(jsonValue);
        var userModel = {"member": jsonValue};
        print(userModel.toString());
        HomeProviderModel.userMemberModel =
            UserMemberModel.fromJson(userModel);

        print(memberInfoModel.email + "  user email");
        SharedPreference.savedMemperInfo(memberInfoModel);
        HomeProviderModel.userName = memberInfoModel.nameEn;
        HomeProviderModel.userEmail = memberInfoModel.email;
        HomeProviderModel.userPicture = memberInfoModel.profilePic ?? "";
        return memberInfoModel;
      } else
        return MemberInfoModel();
    });
  }
}
