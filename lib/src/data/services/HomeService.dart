import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/models/BannerModel.dart';
import 'package:atta_mobile_app/src/data/models/CategoryModel.dart';
import 'package:atta_mobile_app/src/data/models/OfferModel.dart';

import 'package:http/http.dart' as http;

class HomeService  {
  //static final http.Client _client = http.Client();

  @override
  Future<List<CategoryModel>> getAllticketCategory() async {
    return await http.get(
      HomeProviderModel.apiUrl + "category",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        return (jsonValue as List)
            .map((f) => new CategoryModel.fromJson(f))
            .toList();
      } else
        return List<CategoryModel>();
    });
  }

  @override
  Future<List<OfferModel>> getLatestOffers() async {
    return await http.get(
      HomeProviderModel.apiUrl + "Offer/LatestOffers/5",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        List<OfferModel> offers =
            (jsonValue as List).map((f) => new OfferModel.fromJson(f)).toList();

        return offers;
      } else
        return List<OfferModel>();
    });
  }

  @override
  Future<List<OfferModel>> getTopOffer() async {
    return await http.get(
      HomeProviderModel.apiUrl + "Offer/TopOffers/5",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        List<OfferModel> offers =
            (jsonValue as List).map((f) => new OfferModel.fromJson(f)).toList();
        return offers;
      } else
        return List<OfferModel>();
    });
  }

  Future<List<BannerModel>> getListBanner() async {
    print(ApiRoutes.banner);
    print('bearer ${HomeProviderModel.token.toString()}');
    return await http.get(
      ApiRoutes.banner,
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      print(response.body.toString());
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        List<BannerModel> bannerModels =
            (jsonValue as List).map((f) => BannerModel.fromMap(f)).toList();
        return bannerModels;
      } else
        return List<BannerModel>();
    });
  }
}
