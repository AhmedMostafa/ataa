import 'dart:convert';
import 'dart:io';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/models/ShopModel.dart';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class ShopService  {
  @override
  Future<ShopModel> getShopInfo(int id) async {
    return await http.get(
      //TextFieldController.apiUrl + "shop/$id",
      ApiRoutes.shop + "/"+id.toString(),
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        ShopModel model = ShopModel.fromJson(jsonValue);
        return model;
      } else
        return ShopModel();
    });
  }

  Future<File> getFileFromUrl(String url, String dataId) async {
    // check if file exists local
    var dir = await getApplicationDocumentsDirectory();

    File localFile = File("${dir.path}/$dataId.pdf");
    bool isFileExists = await localFile.exists();
    if (isFileExists) {
      localFile.delete();
      return localFile;
    } else {
      // http.Response r = await http.head(url);
      // print("File Size " + r.headers["content-length"]);

      // Dio dio = new Dio();

      // var response = await dio.post(
      //   url,
      //   data: {"aa": "bb" * 22},
      //   onSendProgress: (int sent, int total) {
      //     print("$sent $total");
      //   },
      // );
      var data = await http.get(url);
      var bytes = data.bodyBytes;
      File file = File("${dir.path}/$dataId.pdf");
      File urlFile = await file.writeAsBytes(bytes);
      return urlFile;
    }
  }
}
