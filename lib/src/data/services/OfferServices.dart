import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/src/data/models/OfferModel.dart';

import 'package:http/http.dart' as http;

class OfferService  {
  @override
  Future<List<OfferModel>> getAllOffer() async {
    List<OfferModel> listOfferModel = List<OfferModel>();
    print("offers 0" + HomeProviderModel.token);
    //return
    print(HomeProviderModel.apiUrl + "offer/offers");
    print({'Authorization': 'bearer ${HomeProviderModel.token}'}.toString());
    await http.get(
      HomeProviderModel.apiUrl + "offer/offers", //LatestOffers/0",
//      TextFieldController.apiUrl + "offer",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      print(" code " + response.statusCode.toString());
      print(" body " + response.body.toString());
      print(HomeProviderModel.apiUrl + "offer/offers");
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        print("offers 1" + HomeProviderModel.token);
        listOfferModel =
            (jsonValue as List).map((f) => new OfferModel.fromJson(f)).toList();
        return listOfferModel;
      } else
        print("offers 2" + HomeProviderModel.token);
      return List<OfferModel>();
    });
    return listOfferModel;
  }

  @override
  Future<List<OfferModel>> getCoponsByCategory(int categoryID) async {
    return await http.get(
      HomeProviderModel.apiUrl +
          "Offer/CoponsByCategory/" +
          categoryID.toString(),
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        return (jsonValue as List)
            .map((f) => new OfferModel.fromJson(f))
            .toList();
      } else
        return List<OfferModel>();
    });
  }

  @override
  Future<List<OfferModel>> getAllCopons() async {
    return await http.get(
      HomeProviderModel.apiUrl + "offer/LatestCopons/0",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        return (jsonValue as List)
            .map((f) => new OfferModel.fromJson(f))
            .toList();
      } else
        return List<OfferModel>();
    });
  }
}
