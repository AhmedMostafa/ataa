import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/src/data/models/NewsModel.dart';

import 'package:http/http.dart' as http;

class NewsService  {
  @override
  Future<List<NewsModel>> getAllNews() async {
    return await http.get(
      HomeProviderModel.apiUrl + "News",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        return (jsonValue as List)
            .map((f) => new NewsModel.fromJson(f))
            .toList();
      } else
        return List<NewsModel>();
    });
  }
}
