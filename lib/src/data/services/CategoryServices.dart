import 'dart:convert';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';


import 'package:atta_mobile_app/src/data/models/CityModel.dart';
import 'package:atta_mobile_app/src/data/models/ShopModel.dart';
import 'package:http/http.dart' as http;


class CategoryService {
  @override
  Future<List<ShopModel>> getCategoryById(int categoryID) async {
    print(ApiRoutes.shopsByCategory + categoryID.toString() + " url");
    return await http.get(
      ApiRoutes.shopsByCategory + categoryID.toString(),
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      print(response.statusCode.toString() + "code");
      if (response.statusCode == 200) {
        var jsonValue = json.decode(response.body);
        List<ShopModel> listShop;
        listShop =
            (jsonValue as List).map((f) => new ShopModel.fromJson(f)).toList();
        return listShop;
      } else
        return List<ShopModel>();
    });
  }

  @override
  Future<List<ShopModel>> searchCategories(
    int categoryID,
    int cityID,
    String searchQuery,
  ) async {
    return await http.get(
      ApiRoutes.searchShop +
          categoryID.toString() +
          "/" +
          cityID.toString() +
          "/" +
          searchQuery.toString(),
      headers: {
        'Authorization': 'bearer ${HomeProviderModel.token}',
        //   'content-type': 'application/json',
      },
    ).then((response) {
      print(response.statusCode.toString() + "codeee");
      print(response.body.toString() + "bodyyyy");
      if (response.statusCode == 200 || response.statusCode == 201) {
        var jsonValue = json.decode(response.body);
        List<ShopModel> listModel;
        listModel = (jsonValue['shops'] as List)
            .map((f) => new ShopModel.fromJson(f))
            .toList();
        print(listModel.length.toString() + " aaaa");
        return listModel;
      } else
        return List<ShopModel>();
    });
  }

  Future<List<CityModel>> getCities() async {
    return await http.get(
      ApiRoutes.getCities,
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200 || response.statusCode == 201) {
        var jsonValue = json.decode(response.body);
        return (jsonValue as List)
            .map((f) => new CityModel.fromJson(f))
            .toList();
      } else
        return List<CityModel>();
    });
  }
}
