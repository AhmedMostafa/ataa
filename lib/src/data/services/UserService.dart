import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';

import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/data/models/UserMemberModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';

import 'package:atta_mobile_app/src/data/services/MemberInfoService.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

class UserService  {
  //static final http.Client _client = http.Client();
  GoogleSignInAccount _currentUser;

  @override
  Future<http.Response> signInWithEmailAndPassword(UserModel user) async {
    var model = {"username": user.userName, "password": user.password};
    // print(ApiRoutes.login + " login test");
    user.firebaseToken = HomeProviderModel.firebaseToken;
    // print("Firebase Token: " + user.firebaseToken);
    // print(json.encode(user).toString());
    print(ApiRoutes.login);
    print(json.encode(model.toString()));
    return await http
        .post(
            //TextFieldController.apiUrla + "auth/login",
            ApiRoutes.login,
            headers: {
              "Content-Type": "application/json",
            },
            body: json.encode(model))
        .then((response) {
      print("login " + response.statusCode.toString());
      print("login2 " + response.body.toString());

      return response;
    }).catchError((error) {
      print(error);
    });
  }

  @override
  Future<http.Response> regNewUser(UserModel usermodel) async {
    var model = {
      "email": usermodel.email.trim(),
      "userName": usermodel.userName.trim(),
      "password": usermodel.password,
      "nameEn": usermodel.nameEn.trim(),
      "activationCode": usermodel.studentCode.trim(),
      "telephone": usermodel.telephone.trim(),
      "firebaseToken": HomeProviderModel.firebaseToken
    };
    print(model);
    print(ApiRoutes.register);
    return await http
        .post(
            //TextFieldController.apiUrl + "auth/register",
            ApiRoutes.register,
            headers: {
              "Content-Type": "application/json",
            },
            body: json.encode(model))
        .then((response) {
      print("value " + response.body.toString());
      print("value code " + response.statusCode.toString());
      return response;
      // if (response.statusCode == 200) {
      //   return true;
      // }
      // if (response.statusCode == 404) {
      //   var jsonValue = json.decode(response.body);
      //   TextFieldController.failMSG = jsonValue[0]['description'];
      //   return false;
      // } else
      //   return false;
    }).catchError((error) {
      print(error);
    });
  }

  Future<bool> signInWithFacebook() async {
//    FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
//    try {
    bool returnValue;
    var facebookLogin = new FacebookLogin();
    var result = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        try {
          FacebookAccessToken myToken = result.accessToken;

          print(result.accessToken.toMap().toString() + " map");

//            AuthCredential credential =
//                FacebookAuthProvider.getCredential(accessToken: myToken.token);

          final graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${myToken.token}');
          final profile = json.decode(graphResponse.body);
          print(profile.toString() + " profile");
          print(profile['picture']['data']['url'].toString() + 'url');
          print(myToken.token);
          print(myToken.userId + " user id");
          print(myToken.toMap().toString());
          Map ss = {
            "name": profile['name'],
            "email": profile['email'],
            "provider": "facebook",
            "provider_id": myToken.userId.toString(),
            "image": profile['picture']['data']['url']
          };
          print(ss.toString());
//            FirebaseUser user =
//                await _firebaseAuth.signInWithCredential(credential);

//            print(user.uid + " uid");
          var client = new http.Client();
          await client
              .post(
                ApiRoutes.socialLogin,
                headers: {"Content-Type": "application/json"},
                body: json.encode({
                  "name": profile['name'],
                  "email": profile['email'],
                  "provider": "facebook",
                  "provider_id": myToken.userId.toString(),
                  "image": profile['picture']['data']['url'],
                  "firebaseToken": HomeProviderModel.firebaseToken
                }),
              )
              .timeout(Duration(seconds: 10))
              .whenComplete(client.close)
              .then((response) {
            print(response.statusCode.toString() + " code");

            print(response.body.toString() + "body");
            if (response.statusCode == 200) {
              SharedPreference.savedUserToken(response.body);
              HomeProviderModel.token = json.decode(response.body)["token"];
              // var jsonValue = json.encode(user);
              HomeProviderModel.userMemberModel =
                  UserMemberModel.fromJson(json.decode(response.body));
              print(json.decode(response.body)["member"].toString().trim() +
                  " phone123");
              print(json
                      .decode(response.body)["member"]['telephone']
                      .toString()
                      .trim() +
                  " phone");
              HomeProviderModel.phone = json
                  .decode(response.body)["member"]['telephone']
                  .toString()
                  .trim();
              print(HomeProviderModel.phone.toString() + " phone tttt");
              UserModel user = UserModel();
              print("test 1");
              user.userName = json.decode(response.body)["member"]['email'];
              print("test 2");
              SharedPreference.saveUser(user);
              print("test 3");
              HomeProviderModel.userModel = user;
              print("test 4");
              MemberInfoService().getAllMemberInfo();
              print("test 5");
              print(response.body);
              print("test 6");
              returnValue = true;
            } else
              returnValue = false;
          });
        } catch (e) {
          returnValue = false;
        }
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("no");
        returnValue = false;
        break;
      case FacebookLoginStatus.error:
        print("error");
        returnValue = false;
        break;
    }
    return returnValue;
    //return false;
//    } catch (e) {
//      print(e);
//      print("error");
//      return null;
//    }
//    return null;
  }

  Future<bool> signInWithGoogle() async {
    //FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    print(" test");
    bool returnValue;
    //try {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        'https://www.googleapis.com/auth/contacts.readonly',
      ],
    );
//    print(" so lgo123");

//    await _googleSignIn.signIn();

//    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
//      print(" so lgo");
//      print(account.email + account.toString() + " aaa");
//      _currentUser = account;
//      _handleGetContact();
//    });
//    print(" so lgo122133");
//      AuthCredential credential;
//      FirebaseUser user;
//      String tokenUser;
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    googleUser.authentication.then((googleAuth) async {
      print(googleAuth.toString() + " qwe");
    });
    print(googleUser.toString() + " dddd");
//        credential = GoogleAuthProvider.getCredential(
//            idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
//        user = await _firebaseAuth.signInWithCredential(credential);
////        user.getIdToken().then((token) {
////          print(user.uid + " uid");
////          tokenUser = token;
////          print(" uid2 " + token);
////
////          print("laskdjakl");
////        });
//      });
//
    var client = new http.Client();
    Map ss = {
      "name": googleUser.displayName,
      "email": googleUser.email,
      "provider": "google",
      "image": googleUser.photoUrl.toString(),
      "provider_id": googleUser.id
    };
    print(ss.toString() + "model");
//    return false;
    var response = await client
        .post(
          ApiRoutes.socialLogin,
          headers: {"Content-Type": "application/json"},
          body: json.encode({
            "name": googleUser.displayName,
            "email": googleUser.email,
            "provider": "google",
            "image": googleUser.photoUrl.toString(),
            "provider_id": googleUser.id,
            "firebaseToken": HomeProviderModel.firebaseToken
          }),
        )
        .timeout(Duration(seconds: 10))
        .whenComplete(client.close)
        .then((response) {
      print(response.statusCode.toString() + " code");

      print(response.body.toString() + "body");
      if (response.statusCode == 200) {
        SharedPreference.savedUserToken(response.body);
        HomeProviderModel.token = json.decode(response.body)["token"];
        // var jsonValue = json.encode(user);
        HomeProviderModel.userMemberModel =
            UserMemberModel.fromJson(json.decode(response.body));
        print(json.decode(response.body)["member"].toString().trim() +
            " phone123");
        print(json
                .decode(response.body)["member"]['telephone']
                .toString()
                .trim() +
            " phone");
        HomeProviderModel.phone =
            json.decode(response.body)["member"]['telephone'].toString().trim();
        print(HomeProviderModel.phone.toString() + " phone tttt");
        UserModel user = UserModel();
        print("test 1");
        user.userName = json.decode(response.body)["member"]['email'];
        print("test 2");
        SharedPreference.saveUser(user);
        print("test 3");
        HomeProviderModel.userModel = user;
        print("test 4");
        MemberInfoService().getAllMemberInfo();
        print("test 5");
        print(response.body);
        print("test 6");
        returnValue = true;
      } else
        returnValue = false;
    });

    return returnValue;
  }

  Future<bool> signInWithTwitter() {
    return null;
  }

  Future<bool> updateUser(
      UserMemberModel userMemberModel, phone, userName) async {
    userMemberModel.member.telephone = phone;
    userMemberModel.member.nameEn = userName;
    var userUpdated = userMemberModel;
    print(ApiRoutes.member +
        HomeProviderModel.memberInfoModel.memberId.toString());
    print(userUpdated.toJson().toString());
    return await http
        .put(
            ApiRoutes.member +
                HomeProviderModel.memberInfoModel.memberId.toString(),
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'bearer ${HomeProviderModel.token}'
            },
            body: json.encode(userUpdated.member.toJson()))
        .then((response) {
      print(response.statusCode.toString() + " status code edit profile");

      print(response.body.toString() + " body of edit profile");
      if (response.statusCode == 204) {
        HomeProviderModel.memberInfoModel.nameEn = userUpdated.member.nameEn;
        HomeProviderModel.memberInfoModel.telephone =
            userUpdated.member.telephone;

        HomeProviderModel.userMemberModel = userUpdated;
        return true;
      }
      if (response.statusCode == 404) {
        var jsonValue = json.decode(response.body);
        HomeProviderModel.failMSG = jsonValue[0]['description'];
        return false;
      } else
        return false;
    }).catchError((error) {
      print(error);
    });
  }

  Future<bool> saveUserPicture(File imgFile) async {
    http.MultipartRequest request = new http.MultipartRequest(
        'post',
        Uri.parse(
          ApiRoutes.uploadImageProfile,

          //    TextFieldController.apiUrl + "common/UploadFiles/profile"
        ));
    // request.files.add(imgFile);
    return await http.post(
      //TextFieldController.apiUrl + "common/UploadFiles/profile",
      ApiRoutes.uploadImageProfile,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    ).then((response) {
      if (response.statusCode == 200) {
        return true;
      } else
        return false;
    }).catchError((error) {
      print(error);
    });
  }

  Future<void> _handleGetContact() async {
    final http.Response response = await http.get(
      'https://people.googleapis.com/v1/people/me/connections'
      '?requestMask.includeField=person.names',
      headers: await _currentUser.authHeaders,
    );
    print(response.statusCode.toString() + " code");
    print(response.body + " body");
    if (response.statusCode != 200) {
      print('People API ${response.statusCode} response: ${response.body}');
      return;
    }
    final Map<String, dynamic> data = json.decode(response.body);
  }
}
