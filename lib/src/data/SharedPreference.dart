import 'dart:convert';

import 'package:atta_mobile_app/src/data/models/MemberInfoModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreference {
  UserModel userModel;
  MemberInfoModel memberInfoModel;
  static Future<bool> saveUser(UserModel usermodel) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString("userModel", jsonEncode(usermodel));
  }

// ///
  static Future<UserModel> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return UserModel.fromJson(jsonDecode(prefs.getString("userModel")));
  }

  static Future<bool> savedMemperInfo(MemberInfoModel memberInfo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      await prefs.setInt("memberId", memberInfo.memberId);
      await prefs.setString("telephone", memberInfo.telephone);
      await prefs.setString("email", memberInfo.email);
      await prefs.setString("nameEn", memberInfo.nameEn);
      await prefs.setString("nameEn", memberInfo.nameAr);
      return true;
    } catch (e) {
      print("save to shared faild   :  $e");
      return false;
    }
  }

  static Future<MemberInfoModel> getMemberInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return new MemberInfoModel(
        memberId: prefs.getInt("memberId"),
        telephone: prefs.getString("telephone"),
        email: prefs.getString("email"),
        nameEn: prefs.getString("nameEn"),
        nameAr: prefs.getString("nameAr"));
  }

//  static Future<UserInfoModel> getUserInfoObject() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String pi = prefs.getString('userinfo');
//     UserInfoModel uinfo = UserInfoModel.fromJson(json.decode(pi));
//     return uinfo;
//   }

  // --------------------------------------------------------- -
  static Future<bool> savedUserToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
    return true;
  }

  static Future<String> getUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String pi = prefs.getString('token');
    return pi;
  }

  static Future<bool> removeUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("token");
    return true;
  }
}
