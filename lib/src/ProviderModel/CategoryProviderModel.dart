import 'dart:convert';

import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/data/models/CityModel.dart';
import 'package:atta_mobile_app/src/data/models/MemberInfoModel.dart';
import 'package:atta_mobile_app/src/data/models/ShopModel.dart';
import 'package:atta_mobile_app/src/data/services/CategoryServices.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;





class CategoryProviderModel extends ChangeNotifier {
  //to make single instance of class
  // factory CategoryController() {
  //   if (_this == null) _this = CategoryController._();
  //   return _this;
  // }
  // static CategoryController _this;

  // CategoryController._();

  // static CategoryController get con => _this;
  CityModel selectedCity;

  CategoryService categoryService = CategoryService();
  List<CityModel> cities = new List<CityModel>();
  List<ShopModel> categories;
  List<ShopModel> allCategories = new List<ShopModel>();
  String searchWord;
  int categoryID = 0;
  bool isloading = true;

  void clearData() {
    categories.clear();
    categories = null;
  }

  Future<bool> saveFavoritShop(int shopId) async {
    MemberInfoModel memberInfoModel = await SharedPreference.getMemberInfo();
    var model = {
      "MemberId": memberInfoModel.memberId.toString(),
      "ShopId": shopId.toString()
    };
    return await http.post(
      HomeProviderModel.apiUrl + "FavoriteShop",
      body: json.encode(model),
      headers: {
        'content-type': 'application/json',
        'Authorization': 'bearer ${HomeProviderModel.token}'
      },
    ).then((response) {
      if (response.statusCode == 200 || response.statusCode == 201) {
        return true;
      } else
        return false;
    });
  }

  Future<bool> deleteFavoritShop(int shopId) async {
    MemberInfoModel memberInfoModel = await SharedPreference.getMemberInfo();
    return await http.delete(
      HomeProviderModel.apiUrl +
          "FavoriteShop/${shopId.toString()}/${memberInfoModel.memberId.toString()}",
      headers: {'Authorization': 'bearer ${HomeProviderModel.token}'},
    ).then((response) {
      if (response.statusCode == 200 || response.statusCode == 201) {
        return true;
      } else
        return false;
    });
  }

  Future getCategoryByID(int categoryID) async {
    print(categoryID.toString() + " cat id");
    isloading = true;
  notifyListeners();
    categories = await categoryService.getCategoryById(categoryID);
    // print(selectedCity.cityId.toString() + " city id");
//    categories = await categoryService.searchCategories(
//        categoryID, selectedCity.cityId, searchWord);
    print("getCategoryByID");
    isloading = false;
     notifyListeners();
  }

  init() async {
    isloading = true;
    await getCities();
    await getCategoryByID(categoryID);
    isloading = false;
  }

  searchCategories() async {
    isloading = true;
     notifyListeners();
//    categories.clear();
//    news.clear();
//    events.clear();

    categories = await categoryService.searchCategories(
        categoryID, selectedCity.cityId, searchWord);
    print("search categories");

    isloading = false;
     notifyListeners();
  }

  Future getCities() async {
    //SharedWidget.loading(context);
    isloading = true;
    notifyListeners();
    cities = await categoryService.getCities();
    cities.insert(0, CityModel(cityId: 0, nameAr: "الكل", isSelected: true));
    selectedCity = cities[0];
    getSearchValue();
    //Navigator.pop(context);
    isloading = false;
     notifyListeners();
  }

  Future getSearchValue() async {
    // SharedWidget.loading(context);
    isloading = true;
   notifyListeners();
//    categories.clear();
//    news.clear();
//    events.clear();
    print(categoryID.toString() + " searchhhhh 1");
    print(selectedCity.cityId.toString() + " searchhhhh 2");
    print(searchWord + " searchhhhh");
    categories = await categoryService.searchCategories(
        categoryID, selectedCity.cityId, searchWord);

    print("getSearchValue");
    isloading = false;
     notifyListeners();
  }
}
