import 'dart:convert';
import 'dart:io';
import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:atta_mobile_app/src/ProviderModel/HomeProviderModel.dart';
import 'package:atta_mobile_app/Utils/api_routes.dart';
import 'package:atta_mobile_app/src/data/SharedPreference.dart';
import 'package:atta_mobile_app/src/data/models/UserMemberModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';
import 'package:atta_mobile_app/src/data/services/MemberInfoService.dart';
import 'package:atta_mobile_app/src/data/services/UserService.dart';
import 'package:atta_mobile_app/src/screens/UserMangementScreen/reg_screen.dart';
import 'package:atta_mobile_app/src/widgets/SharedWidget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';


class UserProviderModel extends ChangeNotifier {
  //to make single instance of class
  factory UserProviderModel() {
    if (_this == null) _this = UserProviderModel._();
    return _this;
  }

  static UserProviderModel _this;

  UserProviderModel._();

  static UserProviderModel get con => _this;

  UserService userService = UserService();

  UserModel model = UserModel();

  MemberInfoService memberInfoService = new MemberInfoService();


  File image;
  bool isEditingProfile = false;

  Future getImage() async {
    var _pickedImage = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 600, maxWidth: 600);

    setState(() {
      image = _pickedImage;
      HomeProviderModel.userPicture = _pickedImage.toString();
    });
  }


  Future uploadImage(context, key) async {
    // SharedWidget.onLoading(context);
    String authorizationValue = 'Bearer ' + HomeProviderModel.token;
    Dio dio = new Dio();
    FormData aa = FormData.from({
      "image": new UploadFileInfo(image, "upload1.jpg")
      //,"image": UploadFileInfo(image, image.uri.toFilePath())
    });

    FormData formdata = new FormData(); // just like JS
    formdata.add("image", new UploadFileInfo(image, image.uri.toFilePath()));

    await dio
        .post(ApiRoutes.uploadImageProfile,
            data: aa,
            options: Options(
              headers: {"Authorization": authorizationValue},
            ))
        .catchError((s) {})
        .then((response) async {
      if (response.statusCode == 200) {
        HomeProviderModel.userPicture = response.data[0];

        print(response.data[0]);
        print("تم الحفظ");
        SharedWidget.showToastMsg("تم الحفظ", false);

        Navigator.pop(context);
        return response;
      } else {
        print(response.statusCode.toString() + "status code profile iamge");
        print("خطأ فى تحميل الصوره");

        SharedWidget.showToastMsg("خطأ فى تحميل الصوره", true);
        return null;
        // return null;
      }
    }).catchError((error) {
      print("خطأ فى تحميل الصوره");
      SharedWidget.showToastMsg("خطأ فى تحميل الصوره", true);
      print(error.toString() + "  errore    ");
    });
    // Navigator.pop(context);
  notifyListeners();
  }



  Future signInWithEmailAndPassword(
      BuildContext context,
      GlobalKey<ScaffoldState> loginScaffoldkey,
      GlobalKey<FormState> loginformKey) async {
    final form = loginformKey.currentState;
    HomeProviderModel.loginAutoValid = true;
    if (form.validate()) {
      form.save();
      SharedWidget.onLoading(context);
      http.Response res = await userService.signInWithEmailAndPassword(model);
      if (res.statusCode == 200) {
        SharedPreference.savedUserToken(res.body);
        HomeProviderModel.token = json.decode(res.body)["token"];
        print("API Token: " + HomeProviderModel.token);
        // var jsonValue = json.encode(user);
        HomeProviderModel.userMemberModel =
            UserMemberModel.fromJson(json.decode(res.body));
        print(json.decode(res.body)["member"].toString().trim() + " phone123");
        print(json.decode(res.body)["member"]['telephone'].toString().trim() +
            " phone");
        HomeProviderModel.phone =
            json.decode(res.body)["member"]['telephone'].toString().trim();
//        print(TextFieldController.phone.toString() + " phone tttt");
        // SharedPreference.saveUser(user);
        // TextFieldController.userModel = user;
//        print(TextFieldController.userModel.toJson().toString() + " User");
        SharedPreference.saveUser(model);

        MemberInfoService().getAllMemberInfo();
        HomeProviderModel.isGust = false;
        Navigator.pop(context);
        Navigator.pushNamedAndRemoveUntil(
            context, "/Home", (Route<dynamic> route) => false);
        print(res.body);
        return Response;
      } else if (res.statusCode == 401) {
        bool isdecodeSucceeded = true;
        try {
          var value = jsonDecode(res.body);
          if (isdecodeSucceeded) {
            if (value["title"] != null) {
              SharedWidget.showSnackBar(loginScaffoldkey,
                  "برجاء التأكد من  البريد الالكترونى و كلمة السر الخاصة بك.");
            }
          }
        }
        // on CastError catch (e) {}
        on FormatException catch (e) {
          if (res.body.toString().toLowerCase() ==
              "User is not activated.".toLowerCase()) {
            return showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: SingleChildScrollView(
                      child: Container(
//                        height: MediaQuery.of(context).size.height * 0.4,
//                        width: MediaQuery.of(context).size.width * 0.4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(8),
                                height:
                                    MediaQuery.of(context).size.height * 0.15,
                                child: Center(
                                  child: Text(
                                    "يجب تفعيل الحساب الخاص بك بالبريد الالكتروني\n"
                                    "استمرار كضيف ؟",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10.0),
                                        topRight: Radius.circular(10.0)),
                                    color: Color(0xffFF4E00)),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  FlatButton(
                                    color: FixedAssets.mainColor,
                                    child: Text(
                                      "موافق",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () async {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
//                                  TextFieldController.notActivated = true;
                                      Navigator.pushNamed(context, "/Home");
//                                  Navigator.pushNamedAndRemoveUntil(context,
//                                      "/Home", (Route<dynamic> route) => false);
                                    },
                                  ),
                                  FlatButton(
                                    color: Color(0xffFF4E00),
                                    child: Text(
                                      "الغاء",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () async {
//                                  TextFieldController.notActivated = true;
                                      Navigator.pop(context);
                                      Navigator.pop(context);

                                    },
                                  ),

                                ],
                              ),
                              SizedBox(
                                height: 20,
                              )
                            ],
                          )),
                    ),
                  );
                });
;
          }
        }
      } else {
        print(HomeProviderModel.phone.toString() + " phone login");
        if (HomeProviderModel.phone.toString() == null ||
            HomeProviderModel.phone.toString() == "null" ||
            HomeProviderModel.phone.toString() == "") {
          SharedPreference.removeUserToken();
          Navigator.pushReplacementNamed(context, "/CompleteSignUp");
        } else {
          HomeProviderModel.loginAutoValid = false;
          HomeProviderModel.isGust = false;
          MemberInfoService().getAllMemberInfo();
          Navigator.pushNamedAndRemoveUntil(
              context, "/Home", (Route<dynamic> route) => false);
          HomeProviderModel.notActivated = false;
        }
      }
      Navigator.pop(context);
    }
 notifyListeners();
  }

  Future regNewUser(BuildContext context) async {
    final form = HomeProviderModel.regformKey.currentState;
    HomeProviderModel.regAutoValid = true;

    if (form.validate()) {
      form.save();
      SharedWidget.onLoading(context);
      http.Response res = await userService.regNewUser(model);

      Navigator.pop(context);
      if (res.statusCode == 200) {

        SharedWidget.showToastMsg("تم التسجيل بنجاح", false, time: 5);


        SharedWidget.onLoading(context);

        await userService
            .signInWithEmailAndPassword(model)
            .then((isValidCredentials) async {


          print(HomeProviderModel.phone.toString() + " phone login");
          if (json.decode(res.body)["member"]['telephone'] == null) {
            MemberInfoService().getAllMemberInfo();
            Navigator.pop(context);

            Navigator.pushReplacementNamed(context, "/CompleteSignUp");
          } else {

            SharedPreference.savedUserToken(res.body);
            HomeProviderModel.token =
                json.decode(isValidCredentials.body)["token"].toString();
            print("API Token: " + HomeProviderModel.token);
            // var jsonValue = json.encode(user);
            HomeProviderModel.userMemberModel =
                UserMemberModel.fromJson(json.decode(res.body));
            print(json
                    .decode(isValidCredentials.body)["member"]
                    .toString()
                    .trim() +
                " phone123");
            print(json
                    .decode(isValidCredentials.body)["member"]['telephone']
                    .toString()
                    .trim() +
                " phone");
            HomeProviderModel.phone = json
                .decode(isValidCredentials.body)["member"]['telephone']
                .toString()
                .trim();

            SharedPreference.saveUser(model);

            await MemberInfoService().getAllMemberInfo();
            HomeProviderModel.isGust = false;

            Navigator.pop(context);
            Navigator.pushNamedAndRemoveUntil(
                context, "/Home", (Route<dynamic> route) => false);
          }
//          }
        });
      } else if (res.statusCode == 404) {
//        print("valsss " + res.body.toLowerCase());
        try {
          var value = json.decode(res.body);
          if (value[0]["code"] != null &&
              value[0]["code"] == "DuplicateUserName") {
            SharedWidget.showToastMsg(
                "عفواً, هذا البريد الإلكترونى مسجل بالفعل.", true,
                time: 2);
          }
        } on CastError catch (e) {
          SharedWidget.showToastMsg(res.body, true, time: 2);
        } on FormatException catch (e) {
          if ("${res.body.toString().trim().toLowerCase()}" ==
              "provided email is not a nour university email.") {
            SharedWidget.showToastMsg(
                "عفواً, هذا البريد الإلكترونى غير مسجل بالجامعه.", true,
                time: 2);
          } else if ("${res.body.toString().trim().toLowerCase()}" ==
              "The email that is already associated with an account."
                  .toLowerCase()) {
            SharedWidget.showToastMsg(
                "البريد الإلكتروني مرتبط بالفعل بحساب.", true,
                time: 2);
          } else if ("${res.body.toString().trim().toLowerCase()}" ==
              "The user name that already exists.".toLowerCase()) {
            SharedWidget.showToastMsg("اسم المستخدم الموجود بالفعل.", true,
                time: 2);
          } else if ("${res.body.toString().trim().toLowerCase()}" ==
              "The email that is invalid.".toLowerCase()) {
            SharedWidget.showToastMsg("البريد الإلكتروني غير صالح.", true,
                time: 2);
          } else if ("${res.body.toString().trim().toLowerCase()}" ==
              "The user name that is invalid.".toLowerCase()) {
            SharedWidget.showToastMsg("اسم المستخدم غير صالح.", true, time: 2);
          }
        }
      }

    }
     notifyListeners();
  }

  completeSocial(context, res, GlobalKey<ScaffoldState> loginScaffoldkey) {
    print(res.toString() + " bool value");
    Navigator.pop(context);
    if (!res)
      SharedWidget.showSnackBar(loginScaffoldkey, "خطأ فى التسجيل");
    else {
      print(HomeProviderModel.phone.toString() + " phone login");
      if (HomeProviderModel.phone.toString() == null ||
          HomeProviderModel.phone.toString() == "null" ||
          HomeProviderModel.phone.toString().trim() == "") {
        SharedPreference.removeUserToken();
        Navigator.pushReplacementNamed(context, "/CompleteSignUp");
      } else {
        HomeProviderModel.loginAutoValid = false;
        HomeProviderModel.isGust = false;
        MemberInfoService().getAllMemberInfo();
        // Navigator.pushReplacementNamed(context, "/Home");
        Navigator.pushNamedAndRemoveUntil(
            context, "/Home", (Route<dynamic> route) => false);
      }
    }
    notifyListeners();
  }

  Future socialLogin(BuildContext context, String typeSocialLogin) async {
    SharedWidget.onLoading(context);
   
  }

  Future updateProfile(context, GlobalKey<FormState> editProfileKey,
      GlobalKey<ScaffoldState> key, phone, userName) async {
    final form = editProfileKey.currentState;
    if (form.validate()) {
      form.save();
      SharedWidget.onLoading(context);
      bool res = await userService.updateUser(
          HomeProviderModel.userMemberModel, phone, userName);
      // refresh();
      if (!res) {
        SharedWidget.showToastMsg("خطآ في عمليه التحديث", true);
        Navigator.pop(context);
      } else {
//        SharedWidget.showSnackBar(
//            TextFieldController.regScaffoldkey, "تم التسجيل بنجاح");
        SharedWidget.showToastMsg("تم التحديث بنجاح", false);
        Navigator.pop(context);
        Navigator.pop(context);
      }
    }
     notifyListeners();
  }

  void navigateToRegPage(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => RegScreen()));
  }

  @override
  void setState(fn) {
  
     notifyListeners();
  }
}
