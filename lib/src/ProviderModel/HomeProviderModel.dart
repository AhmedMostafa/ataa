import 'package:atta_mobile_app/src/data/models/BannerModel.dart';
import 'package:atta_mobile_app/src/data/models/CategoryModel.dart';
import 'package:atta_mobile_app/src/data/models/MemberInfoModel.dart';
import 'package:atta_mobile_app/src/data/models/OfferModel.dart';
import 'package:atta_mobile_app/src/data/models/UserMemberModel.dart';
import 'package:atta_mobile_app/src/data/models/UserModel.dart';
import 'package:atta_mobile_app/src/data/services/HomeService.dart';
import 'package:atta_mobile_app/src/screens/UserMangementScreen/login_screen.dart';
import 'package:flutter/material.dart';


class HomeProviderModel extends ChangeNotifier {
  HomeService homeService = HomeService();

  factory HomeProviderModel() {
    if (_this == null) _this = HomeProviderModel._();
    return _this;
  }
  static HomeProviderModel _this;

  HomeProviderModel._();

  static HomeProviderModel get con => _this;
  bool viewAllCategory = false;
CategoryModel categoryModel=CategoryModel();
  List<CategoryModel> allCategory = new List<CategoryModel>();
  List<OfferModel> topOffer = new List<OfferModel>();
  List<OfferModel> latestOffer = new List<OfferModel>();
  List<BannerModel> listBanner = List<BannerModel>();
//  bool isLoading = false;
  bool topOfferLoading = false;
  bool allCategoryLoading = false;
  bool latestOfferLoading = false;
  bool listBannerLoading = false;
  void init(BuildContext context) async {
    allCategory.clear();
    topOffer.clear();
    latestOffer.clear();
    listBanner.clear();
     notifyListeners();
    await Future.wait([
      getGetAllCategory(),
      getTopOffer(),
      getLatestOffers(),
      getListBanner(),
    ]);
    notifyListeners();
  }

  Future getGetAllCategory() async {
    allCategoryLoading = true;
    allCategory = await homeService.getAllticketCategory();
    allCategoryLoading = false;
  }

  Future getListBanner() async {
    listBannerLoading = true;
    listBanner = await homeService.getListBanner();
    listBannerLoading = false;
  }

  void viewCategory() {
    viewAllCategory = !viewAllCategory;
     notifyListeners();
  }

  Future getTopOffer() async {
    topOfferLoading = true;
    topOffer = await homeService.getTopOffer();
    topOfferLoading = false;
  }

  Future getLatestOffers() async {
    latestOfferLoading = true;
    latestOffer = await homeService.getLatestOffers();
    latestOfferLoading = false;
  }
   static String apiUrl = "http://admin.ataa-pnu.com/api/";
  //"http://198.38.94.188/api/";
  static MemberInfoModel memberInfoModel;
  static UserModel userModel;
  static bool isGust = true;
  static String failMSG = null;
  static UserMemberModel userMemberModel;
  static String firebaseToken;
  //static String url = "http://ataa.dubai-eg.com/";
  static String token = "";
  static String phone;

  //loginPage
//  static final FocusNode focusPasswordField = new FocusNode();
//  static final GlobalKey<FormState> loginformKey = GlobalKey<FormState>();
//  static GlobalKey<ScaffoldState> loginScaffoldkey =
//      new GlobalKey<ScaffoldState>(debugLabel: 'loginkey');
  static bool loginAutoValid = false;
  static bool noSearch = false;
  static bool emptyFav = false;
  static bool isOffer = false;
  static bool notActivated = false;

  //regPage
  static final GlobalKey<FormState> regformKey = GlobalKey<FormState>();
  static final FocusNode focusUserNameField = new FocusNode();
  static final FocusNode focusEmailField = new FocusNode();
  static final FocusNode focusPhoneField = new FocusNode();
  static final FocusNode focusRegPasswordField = new FocusNode();
  static final FocusNode focusRetypePasswordField = new FocusNode();
  static final FocusNode focusCodeField = new FocusNode();
  static final TextEditingController passwordController =
      new TextEditingController();
  static final TextEditingController emailController =
      new TextEditingController();
  static final TextEditingController telephoneController =
      new TextEditingController();
  static final TextEditingController studenCodeController =
      new TextEditingController();
  static final TextEditingController usernmeController =
      new TextEditingController();

  static final TextEditingController searchController =
      new TextEditingController();
  static final TextEditingController noOfAttendes = new TextEditingController();
  static bool regAutoValid = false;
  static final GlobalKey<ScaffoldState> regScaffoldkey =
      new GlobalKey<ScaffoldState>();

  //layout
  static final GlobalKey<ScaffoldState> layoutScaffoldkey =
      GlobalKey<ScaffoldState>();

  //layoutCOntext
  static BuildContext layoutContext;

  //ticket page
  static int categoryID = 0;
  static String appBar = "";
  static String userName = "";
  static String userEmail = "";
  static String userPicture = "";

  static mustLoginView(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen(true)));
  }

  static returnHome(context) {
    Navigator.pushReplacementNamed(context, "/Home");
  }

  // static Future<bool> _willPopCallback(context) async {
  //   Navigator.pushReplacementNamed(context, "/Home");
  //   return true;
  // }

  static cleanUserInfo() {
    memberInfoModel = null;
    userModel = null;
    isGust = true;
    failMSG = null;
    userMemberModel = null;

    token = null;
    phone = null;
    categoryID = 0;
    appBar = "";
    userName = "";
    userEmail = "";
    userPicture = "";
    firebaseToken = null;
  }
}
