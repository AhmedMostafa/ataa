
import 'package:atta_mobile_app/src/screens/HomeScreen/home_screen.dart';
import 'package:flutter/material.dart';


class LayoutProviderModel extends ChangeNotifier {
  //to make single instance of class
  factory LayoutProviderModel() {
    if (_this == null) _this = LayoutProviderModel._();
    return _this;
  }
  static LayoutProviderModel _this;

  LayoutProviderModel._();

  static LayoutProviderModel get con => _this;
  // int get _currentTab => currentTab;
  int currentTab = 0;

  List<Widget> screens = <Widget>[
    HomeScreen(),
    HomeScreen(),
    HomeScreen(),
    // TicketScreen(),
    // OfferScreen(),
  ];

  void setCurrentTab(int index) {
    currentTab = index;
  
      currentTab = index;
 
   notifyListeners();
  }

  ///int get currentTab => currentTab;
}
