import 'package:atta_mobile_app/Utils/FixedAssets.dart';
import 'package:atta_mobile_app/src/screens/UserMangementScreen/CompleteSignUp.dart';
import 'package:atta_mobile_app/src/screens/UserMangementScreen/ForgetPasswordScreen.dart';
import 'package:atta_mobile_app/src/screens/UserMangementScreen/login_screen.dart';
import 'package:atta_mobile_app/src/screens/splash_screen.dart';
import 'package:atta_mobile_app/src/widgets/layout.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'ProviderModel/CategoryProviderModel.dart';
import 'ProviderModel/HomeProviderModel.dart';
import 'ProviderModel/LayoutProviderModel.dart';
import 'ProviderModel/UserProviderModel.dart';
import 'helper/HexColor.dart';


class MyApp extends StatelessWidget {
  // static MaterialApp _app;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final appTheme = ThemeData(
    brightness: Brightness.light,
    fontFamily: 'DIN',
   
  );

  Widget build(BuildContext context) {
    return
    
       MultiProvider(
          providers: [
          ChangeNotifierProvider(
            builder: (_) => UserProviderModel(),
                
          ),
        
             ChangeNotifierProvider(
            builder: (_) => HomeProviderModel(),
                
          ),
            ChangeNotifierProvider(
            builder: (_) => CategoryProviderModel(),
                
          ),
            ChangeNotifierProvider(
            builder: (_) => LayoutProviderModel(),
                
          ),
          
          
        ],
         child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'عطاء',
            theme: appTheme,
            home: OfflineBuilder(
              connectivityBuilder: (BuildContext context,
                  ConnectivityResult connectivity, Widget child) {
                _firebaseMessaging.configure(
                  onMessage: (Map<String, dynamic> msg) async {
                    //if (msg != null) {
                    print("done");
                    //}
                    print((msg["data"].toString() + " onMessagecalled"));
                    print((msg.toString() + " onMessagecalled"));
                  },
                  onLaunch: (Map<String, dynamic> msg) async {
                    print("done");
                    print((msg["data"].toString() + " onMessagecalled"));
                    print(("onLaunch called"));
                  },
                  onResume: (Map<String, dynamic> msg) async {
                    //if (msg != null) {
                    print("done");
                    //}
                    print((msg["data"].toString() + " onMessagecalled"));
                    print(("onResume called"));
                  },
                );
                _firebaseMessaging.requestNotificationPermissions(
                    const IosNotificationSettings(
                        alert: true, badge: true, sound: true));
                _firebaseMessaging.onIosSettingsRegistered
                    .listen((IosNotificationSettings settings) {
                  print(" iso setting Registered");
                });

                _firebaseMessaging.getToken().then((fff) {
                  HomeProviderModel.firebaseToken = fff;
                  print(fff.toString() + " firebase token");
                });
                final bool connected = connectivity != ConnectivityResult.none;
                return new Stack(
                  fit: StackFit.expand,
                  children: [
                    Center(
                        child: connected
                            ? SplashScreen()
                            : Scaffold(
                                body: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Opacity(
                                          opacity: 0.6,
                                          child: Image.asset(
                                            FixedAssets.noWifi,
                                            scale: 3,
                                          )),
                                      SizedBox(
                                        height: 25,
                                      ),
                                      Text(
                                        'يرجي الاتصال بالإنترنت لعرض المحتوى..',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 19,
                                            color: HexColor("#ED949494"),
                                            fontWeight: FontWeight.w300),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                  ],
                );
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    'There are no bottons to push :)',
                  ),
                ],
              ),
            ),
            routes: <String, WidgetBuilder>{
              "/Splash": (context) => SplashScreen(),
              "/Home": (context) => LayoutScreen(),
              "/Login": (context) => LoginScreen(false),
              "/MustLogin": (context) => LoginScreen(true),
              
              '/ForgetPassword': (context) => ForgetPasswordScreen(),
              "/CompleteSignUp": (context) => CompleteSignUp(),
            },
            builder: (BuildContext context, Widget child) {
              return Directionality(
                textDirection: TextDirection.rtl,
                child: Builder(
                  builder: (BuildContext context) {
                    return MediaQuery(
                        data: MediaQuery.of(context).copyWith(
                          textScaleFactor: 1.0,
                        ),
                        child: child);
                  },
                ),
              );
            }
    ),
       );
  
  }
}
