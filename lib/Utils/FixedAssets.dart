import 'package:flutter/material.dart';

class FixedAssets {
  static String assetsFile = "assets/imgs/";
  static String noImage = assetsFile + "noimage.png";
  static String placeHolder = assetsFile + "placeHolder.gif";

  static String logo = assetsFile + "logoo.png";
  static String disc = assetsFile + "discount_cut.png";
  static String gdisc = assetsFile + "greendisc.png";
  static String scissors = assetsFile + "scissors.png";
  static String discount = assetsFile + "discount1.png";

  static String noWifi = assetsFile + "noWifi.png";
  static Color mainColor = Color(0xffFF4E00);
}
