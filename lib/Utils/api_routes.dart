class ApiRoutes {
  static const String BaseUrl = "http://admin.ataa-pnu.com/";
//      "http://198.38.94.188/"; //"http://ataa.dubai-eg.com/";
  static const String api = BaseUrl + "api/";
  static const String auth = api + "auth/";
  static const String login = auth + "login";
  static const String register = auth + "register";
  static const String uploadImageProfile = api + "common/UploadFiles/profile";
  static const String checkToken = auth + "checkToken";
  static const String MemberInfoService = api + "Member/MemberByUser/";
  static const String event = api + "Event";
  static const String favoriteEvent = api + "FavoriteEvent/";
  static const String memberEvent = api + "MemberEvent";
  static const String member = api + "Member/";

  static const String contact = api + "contact";
  static const String shop = api + "shop";
  static const String getCities = api + "City";
  static const String restPassword = auth + "ResetPassword/";
  static const String socialLogin = auth + "socialLogin";
  static const String search = api + "search/globalsearch/";
  static const String updateProfile = api + "Member/CompleteMemberProfile/";
  static const String shopsByCategory = api + "shop/ShopsByCategory/";
  static const String searchShop = search + "Shop/";
  static const String logout = auth + "logout/";
  static const String changePassword = auth + "ChangePassword";
  static const String banner = api + "Banner";
}
